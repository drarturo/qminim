<?php

/**
 * @file private_page.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */

if (!defined('GATE_PASSED')) exit();

$trial = get_trial_instance();
$smarty->assign("testmode", $trial->testmode);
$action = get_user_var('action', 'index_page');

if ($trial->status != 'Setup') {
    if (strstr($action, "treatment")) {
        if (strstr($action, "add") || strstr($action, "delete"))
            $action = "index_page";
    }
    if (strstr($action, "factor")) {
        if (strstr($action, "add") || strstr($action, "delete"))
            $action = "index_page";
    }
}

if (isset($_SESSION['on_init'])) {
    $action = $_SESSION['on_init'];
    unset($_SESSION['on_init']);
}

switch($action) {
    case "deletetrial":
        $db_file = FILES_DIR . $_POST['db_file'];
        unlink($db_file);
	    display_index_page('trials.tpl');
        break;
    case "uploadnew":
        $_SESSION['on_init'] = 'uploadtrial';
        $db_file = FILES_DIR . $_POST['db_file'];
        $_SESSION['db_file'] = $db_file;
        header("Location: " . BASE_URL . 'index.php');
        break;
    case "selectndownload":
        $_SESSION['on_init'] = 'showdownload';
    case "selecttrial":
    case "addtrial":
        unset($_SESSION['subjectsfilter']);
        $db_file = FILES_DIR . $_POST['db_file'];
        $_SESSION['db_file'] = $db_file;
        header("Location: " . BASE_URL . 'index.php');
        break;
    case "wide_screen_toggle":
        toggle_user_screen();
        break;
	case "addtreatment":
	    if ($trial->status == 'Setup')
		    $smarty->display('add_treatment.tpl');
		break;
	case "treatmentadded":
	    treatment_added();
	    break;
    case "edittreatment";
        $smarty->assign('treatment', strip_custom($_POST['treatment']));
        $smarty->assign('initialtreatment', strip_custom($_POST['initialtreatment']));
        $smarty->assign('ratio', $trial->treatments[strip_custom($_POST['treatment'])]);
        $smarty->display('edit_treatment.tpl');
        break;
    case "treatmentedited":
        treatment_edited();
        break;
    case "deletetreatment":
        delete_treatment();
        break;
    case "addfactor":
        $smarty->assign('factor', '');
        $smarty->assign('weight', 1);
        $smarty->assign("status", $trial->status);
        $smarty->assign('levels', array());
		$smarty->display('add_factor.tpl');
		break;
	case "factoradded":
	    factor_added();
	    break;
    case "editfactor":
        $smarty->assign("status", $trial->status);
        $smarty->assign('factor', strip_custom($_POST['factor']));
        $smarty->assign('initialfactor', strip_custom($_POST['initialfactor']));
        $smarty->assign('levels', $trial->factors[strip_custom($_POST['factor'])][1]);
        $smarty->assign('weight', $trial->factors[strip_custom($_POST['factor'])][0]);
        $smarty->display('edit_factor.tpl');
        break;
    case "factoredited":
        $smarty->assign("status", $trial->status);
        factor_edited();
        break;
    case "deletefactor":
        delete_factor();
        break;
    case "editsettings":
        $smarty->assign("prob_arr", array('Biased Coin' => 'Biased Coin', 'Naive' => 'Naive'));
        $smarty->assign("dist_arr", array('Marginal Balance' => 'Marginal Balance', 'Range' => 'Range', 'Standard Deviation' => 'Standard Deviation', 'Variance' => 'Variance'));
        $smarty->assign("id_type_arr", array('Numeric' => 'Numeric', 'Alpha' => 'Apha', 'Alphanumeric' => 'Alphanumeric'));
        $smarty->assign("id_order_arr", array('Sequential' => 'Sequential', 'Random' => 'Random'));
        $smarty->assign("title", $trial->title);
        $smarty->assign("prob_method", $trial->prob_method);
        $smarty->assign("dist_method", $trial->dist_method);
        $smarty->assign("base_prob", $trial->base_prob);
        $smarty->assign("status", $trial->status);
        $smarty->assign("sample_size", $trial->sample_size);
        $smarty->assign("id_type", $trial->id_type);
        $smarty->assign("id_order", $trial->id_order);
        $smarty->assign("recycle_ids", $trial->recycle_ids);
        $smarty->display('edit_settings.tpl');
        break;
    case "settingsedited":
        settings_edited();
        break;
    case "newsamplesize":
        new_sample_size();
        break;
    case "editpreload":
        $smarty->assign("pgrand_total", $trial->get_grand_total($trial->preload));
        $smarty->assign("factors", $trial->factors);
        $smarty->assign("treatments", $trial->treatments);
        $smarty->assign("preload", $trial->preload);
        $smarty->assign("preloadtp", $trial->get_preload_transpose());
        $smarty->assign("preloadtype", $_POST['preloadtype']);
        $smarty->display('edit_preload.tpl');
        break;
    case "preloadedited":
        preload_edited();
        break;
    case "clearpreload":
        $trial->clear_preload();
	    display_index_page('preload.tpl');
        break;
    case "addsubject":
        $a = array();
        $b = array();
        foreach ($trial->factors as $f => $v) {
            $levels = array_merge((array)'---------', (array)$v[1]);
            $a[$f] = array_combine($levels, $levels);
            if ($trial->testmode)
                $b[$f] = $v[1][array_rand($v[1])];
            else
                $b[$f] = '---------';
        }
        $smarty->assign('options_arr', $a);
        $smarty->assign('selects_arr', $b);
        $smarty->assign('factors', $trial->factors);
        $smarty->display('add_subject.tpl');
        break;
    case "subjectadded":
        subject_added();
        break;
    case "enrollall":
        enroll_all();
        break;
    case "editsubject":
        $a = array();
        $b = array();
        $c = array_combine(array_keys($trial->treatments), array_keys($trial->treatments));
        $id = strip_custom($_POST['id']);
        $subject = $trial->subjects[$id];
        foreach ($trial->factors as $f => $levels) {
            $a[$f] = array_combine($levels[1], $levels[1]);
            $b[$f] = $subject[$f];
        }
        $smarty->assign('id', $id);
        $smarty->assign('options_arr', $a);
        $smarty->assign('selects_arr', $b);
        $smarty->assign('treatments_arr', $c);
        $smarty->assign('treatment', $subject['treatment']);
        $smarty->assign('factors', $trial->factors);
        if (isset($_POST['currentpage']))
            $smarty->assign('currentpage', strip_custom($_POST['currentpage']));
        $smarty->display('edit_subject.tpl');
        break;
    case "subjectedited":
        subject_edited();
        break;
    case "deletesubject":
        delete_subject();
        break;
	case "showsupport":
	    $smarty->assign('supporttypes', get_supporttypes());
	    $post = array('supporttype' => 'Help', 'subject_text' => '', 'comment' => '');
	    $smarty->assign('post', $post);
	    $smarty->display("support.tpl");
	    break;
	case "showhelp":
	    $smarty->display("help.tpl");
	    break;
	case "support_data":
	    manage_support_data();
		break;
	case "logoutas":
	    manage_logout_as();
	    break;
	case "logout":
        $smarty->clear_all_assign();
        $smarty->clear_all_cache();
        session_unset();
        header("Location: " . BASE_URL . 'index.php');
        break;
	case "showdownload":
	    show_download();
	    break;
    case "download":
        download_trial();
        break;
    case "uploadtrial":
        $smarty->assign('max_file_size', Settings::getSetting('max_trial_file_size'));
        $smarty->display('upload_trial.tpl');
        break;
    case "trialuploaded":
        trial_uploaded();
        break;
    case "applytrialupload":
        apply_trial_upload();
        break;
    case "resettrial":
        reset_trial();
        break;
    case "trialsummary":
        $smarty->assign('printonly', 0);
        display_index_page('trial_summary.tpl');
        break;
    case "printtrialsummary":
        $smarty->assign('printonly', 1);
        echo display_index_page('trial_summary.tpl', true);
        break;
    case "save_as_preload":
        save_as_preload();
        break;
    case "clear_error_logs":
        file_put_contents(FILES_DIR . 'error.log', '');
        $smarty->display('error_logs.tpl');
        break;
    case "show_error_logs":
        if (!file_exists(FILES_DIR . 'error.log'))
            file_put_contents(FILES_DIR . 'error.log', '');
        $smarty->display('error_logs.tpl');
        break;
    case "clear_template_cache":
        $smarty->clear_compiled_tpl();
        show_admin_panel();
        break;
    case "clear_data_cache":
        $smarty->clear_all_cache();
        show_admin_panel();
        break;
    case "system_information":
        $smarty->display('site_info.tpl');
        break;
    case "email_form_data":
        email_form_data();
        break;
    case "addsitesetting":
        add_site_setting();
        break;
    case "editsitesetting":
        edit_site_setting();
        break;
    case "deletesitesetting":
        delete_site_setting();
        break;
    case "changesettingname":
        change_setting_name();
        break;
    case "changesettingvalue":
        change_setting_value();
        break;
    case "changesettingtype":
        change_setting_type();
        break; 
    case "increaseadminrank":
        increase_admin_rank();
        break; 
    case "admintoggle":
        admin_toggle();
        break;
    case "admin_panel":
        show_admin_panel();
        break;
    case "site_users":
        show_site_users();
        break;
    case "site_configs":
        $smarty->display('site_configs.tpl');
        break;
    case "disable user":
    case "enable user":
        toggle_user_status();
        break;
    case "deleteuser":
        delete_user();
        break;
    case "deleterequestemail":
        delete_request_email();
        show_site_users();
        break;
    case "edituser":
        edit_user();
        break;
    case "user_edited":
        manage_user_edited();
        break;
    case "changeusername":
        change_user_name();
        break;
    case "repairpassword":
        repair_password();
        break;
    case "adduser":
        $post = Array ("user_source" => 'new', "password" => "", "salutation" => "", "job_title" => "0", "first_name" => "", "last_name" => "", "email" => "", "city" => "", "country" => "", "affiliation" => "", "user_name" => "");
        if (isset($_POST['email'])) {
            $post['email'] = $_POST['email'];
            $post['user_source'] = 'requested';
        }
        $smarty->assign('post', $post);
  	    $smarty->assign('salutations', get_salutation());
	    $smarty->assign('job_titles', get_job_titles());
	    $smarty->assign('countries', get_countries());
        $smarty->display('add_user.tpl');
        break;
    case "useradded":
        manage_added_user();
        break;
    case "loginas":
        manage_login_as();
        break;
    case "sendmsg":
        manage_send_message();
        break;
    case "message_sent":
        manage_message_sent();
        break;
    case "edit_profile":
  	    $smarty->assign('salutations', get_salutation());
	    $smarty->assign('job_titles', get_job_titles());
	    $smarty->assign('countries', get_countries());
	    $smarty->assign('post', get_profile_data());
        $smarty->display('edit_profile.tpl');
        break;
    case "profile_edited":
        manage_profile_data();
        break;
    case "change_password":
        $smarty->display('change_password.tpl');
        break;
    case "password_changed":
        manage_change_password();
        break;
    case "index_page":
        if (Settings::isAdmin($_SESSION['userloggedin']['user_name']) && Settings::getSetting('admin_show_admin', false))
            show_site_users();
        else
	        display_index_page();
        break;
    case "site_settings":
        display_site_settings();
        break;
    case "set_subjects_filter":
        set_subjects_filter();
        display_index_page();
        break;
    case "set_subjects_filter_from_freq":
        set_subjects_filter_from_freq();
        display_index_page();
        break;
    case "repair_user_db":
        repair_user_db();
        break;
    case "select_subject_id":
        set_subjects_filter();
        display_index_page();
        break;
    case "toggle_include_preload":
        $_SESSION['subjectsfilter']['include_preload'] = !$_SESSION['subjectsfilter']['include_preload'];
        display_index_page();
        break;
}
?>
