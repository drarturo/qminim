<?php

function isCommandLineInterface()
{
    return (php_sapi_name() === 'cli');
}

if (! isCommandLineInterface()) exit();
if (isset($argv[1]) && is_numeric($argv[1]))
    file_put_contents('OUT_OF_SERVICE', time() + $argv[1])
?>
