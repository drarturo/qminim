<?php

/**
 * @file init.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */

if (!defined('GATE_PASSED')) exit();

require_once(BASE_DIR . "include/err_handel.php");
require_once(BASE_DIR . 'include/functions.inc.php');
require_once(BASE_DIR . 'include/test_trial.php');
require_once(BASE_DIR . 'include/trial.php');
require_once(BASE_DIR . 'include/mailer.php');
require_once(BASE_DIR . 'include/user.php');
require_once(BASE_DIR . 'include/admin.php');
require_once(BASE_DIR . 'include/utils.php');
require_once(BASE_DIR . 'include/smarty_funcs.php');
require_once(BASE_DIR . 'include/file.php');
require_once(BASE_DIR . 'include/treatment.php');
require_once(BASE_DIR . 'include/factor.php');
require_once(BASE_DIR . 'include/trial_setting.php');
require_once(BASE_DIR . 'include/preload.php');
require_once(BASE_DIR . 'include/subject.php');
require_once(BASE_DIR . 'include/security.php');
require_once(BASE_DIR . 'include/db.php');
require_once(BASE_DIR . 'include/settings.php');
require_once(BASE_DIR . 'include/UserDAO.php');

require_once(SMARTY_DIR . 'Smarty.class.php');
require_once(SWIFT_DIR . 'swift_required.php');
include_once(BASE_DIR . 'lib/securimage/securimage.php');

ini_set('post_max_size', Settings::getSetting('post_max_size'));

ini_set('upload_max_filesize', Settings::getSetting('upload_max_filesize'));

$smarty = new Smarty();
$smarty->template_dir = TEMPLATE_DIR;
$ret = true;
if (!file_exists(CACHE_DIR)) {
    $ret = $ret && mkdir(CACHE_DIR . 't_cache', 0777, true);
    $ret = $ret && mkdir(CACHE_DIR . 't_compile', 0777, true);
    $ret = $ret && mkdir(CACHE_DIR . 't_config', 0777, true);
}
if (! $ret)
    die('<br />Cache folder!');
$smarty->compile_dir = CACHE_DIR . 't_compile/';
$smarty->config_dir = CACHE_DIR . 't_config/';
$smarty->cache_dir = CACHE_DIR . 't_cache/';

if (class_exists('SQLite3'))
    $smarty->assign("sqlite_version", '3');
elseif (class_exists('SQLiteDatabase'))
    $smarty->assign("sqlite_version", '2');
$smarty->assign("smarty_dir", SMARTY_DIR);
$smarty->assign("swift_dir", SWIFT_DIR);
$smarty->assign("template_dir", TEMPLATE_DIR);
$smarty->assign("cache_dir", CACHE_DIR);
$smarty->assign("users_db_file", USERS_DB_FILE);
$smarty->assign("settings_db_file", SETTINGS_DB_FILE);
$smarty->assign("files_dir", FILES_DIR);
$smarty->assign("base_dir", BASE_DIR);
$smarty->assign("base_url", BASE_URL);
$page_title = Settings::getSetting('page_title');
$smarty->assign('page_title_sanitize', $page_title);
if ($page_title_link = Settings::getSetting('page_title_link', false))
    $smarty->assign('page_title', "<a href='http://$page_title_link' target='_self'>$page_title</a>");
else
    $smarty->assign('page_title', $page_title);
$page_sub_title = Settings::getSetting('page_sub_title');
if ($page_sub_title_link = Settings::getSetting('page_sub_title_link', false))
    $smarty->assign('page_sub_title', "<a href='http://$page_sub_title_link' target='_blank'>$page_sub_title</a>");
else
    $smarty->assign('page_sub_title', $page_sub_title);
if (! isset($_SESSION['userloggedin']))
    $_SESSION['widescreen'] = Settings::getSetting('wide_screen_default');
else
    $_SESSION['widescreen'] = $_SESSION['userloggedin']['wide_screen'];
//$user_agent = $_SERVER['HTTP_USER_AGENT'];
if ($_SESSION['widescreen'])
    $smarty->assign("set_width", true);
else
    $smarty->assign("set_width", false);
$smarty->register_function('array_count', 'get_array_count');
$smarty->register_function('arr_sum', 'get_array_sum');
$smarty->register_function('submit_link', 'submit_form');
$smarty->register_function('paging', 'get_paging_block');
$smarty->register_function('csrf', 'get_csrf');
$smarty->register_function('selmenu', 'selectmenu');
$smarty->register_function('sps', 'subjectprop');
$smarty->register_function('ups', 'userprop');
$smarty->register_modifier('assign', 'smarty_assign');
$smarty->register_modifier('tts', 'tooltipsticky');
?>
