{*
 * index.tpl
 *}
{array_count|assign:sbs arr=$subjects}
{include file='header.tpl'}
<div align="center">
<h2>Subject Enrolled Successfully</h2>
<table>
<tr>
<td>
<ul>
    {foreach from=$subject_keys item=key}
        <li>{$key}: {$subject.$key}</li>
    {/foreach}
</ul>
</td>
</tr>
<tr>
<td>Enrolled to <b>{$subject.treatment}</b></td>
</tr>
<tr>
<td><i>{$sbs} of {$sample_size} subject{if $sbs > 1}s{/if} enrolled</i></td>
</tr>
<tr>
<td>{submit_link action=deletesubject undolast=1 id=$subject.id label=Undo currentpage='last'}&nbsp;
{submit_link action=index_page label=Done subpage="subjects.tpl" tag=button currentpage='last' scroll_to_end='1'}</td>
</tr>
</table>
</div>
{include file='footer.tpl'}
