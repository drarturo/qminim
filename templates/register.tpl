{*
 * register.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{else}
<h3>Registration Form</h3>
<div class="warning">Registration is free, but is required for using minimisation service</div>
{/if}
<form action="index.php" method="post" name="register_form" onsubmit="return valid_register_form();">
{csrf}
<input type="hidden" name="action" value="register_data">
<input type="hidden" name="email" id="email_id" value="{$post.email}">
<div align="center" class="register">
<table>
    <tr>
        <td>
            <h4>{$post.email}</h4>
        </td>
    </tr>
	<tr>
		<td>Title:{$astrisk}<br />
		{if isset($post) and isset($post.salutation)}
		    {html_options name=salutation id=salutation_id options=$salutations selected=$post.salutation}
	    {else}
		    {html_options name=salutation id=salutation_id options=$salutations}
	    {/if}
	</tr>
	<tr>
		<td>Job Title:{$astrisk}<br />
		{if isset($post) and isset($post.job_title)}
		    {html_options name=job_title id=job_title_id options=$job_titles selected=$post.job_title}
	    {else}
		    {html_options name=job_title id=job_title_id options=$job_titles}
	    {/if}
	</tr>
	<tr>
		<td>First Name:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="first_name" id="first_name_id" value="{if isset($post) and isset($post.first_name)}{$post.first_name}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Last Name:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="last_name" id="last_name_id" value="{if isset($post) and isset($post.last_name)}{$post.last_name}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>City:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="city" id="city_id" value="{if isset($post) and isset($post.city)}{$post.city}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Country:{$astrisk}<br />
		{if isset($post) and isset($post.country)}
		    {html_options name=country id=country_id options=$countries selected=$post.country}
	    {else}
		    {html_options name=country id=country_id options=$countries}
	    {/if}
		</td>
	</tr>
	<tr>
		<td>Affiliation:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="affiliation" id="affiliation_id" value="{if isset($post) and isset($post.affiliation)}{$post.affiliation}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Username:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="user_name" id="user_name_id" value="{if isset($post) and isset($post.user_name)}{$post.user_name}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Password:{$astrisk}<input onChange="trim_value(this);" type="button" value="Random Password" onclick="insert_random_password('password_id', 'password_confirm_id', {$rand_pw_len}, 1);" /><br />
		<input onChange="trim_value(this);" type="password" name="password" id="password_id" value="{if isset($post) and isset($post.password)}{$post.password}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Confirm:{$astrisk}<br />
		<input onChange="trim_value(this);" type="password" name="password_confirm" id="password_confirm_id" value="{if isset($post) and isset($post.password_confirm)}{$post.password_confirm}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Comment: <input readonly type="text" name="CharsLeft" id="CharsLeft_id" size="1" value="{$max_register_chars}">characters remained!<br />
        <textarea cols="80" rows="8" id="comment_id" name="comment"
            onBlur="InputLengthCheck({$max_register_chars});"
            onKeyUp="InputLengthCheck({$max_register_chars});">{if isset($post) and isset($post.comment)}{$post.comment}{/if}</textarea><br />
		</td>
	</tr>
	<tr>
		<td>Terms and Conditions: <br />
		<textarea name='eula' readonly="readonly" rows='8' cols='70' id="eula_id">{$eula}</textarea>
		<br />{$astrisk}<input type="checkbox" name="eulacheck" id="eulacheck_id" value="agree" {if isset($post) and isset($post.eulacheck)}{if $post.eulacheck}checked="checked" {/if}{/if}> I agree with above terms and conditions
		</td>
	</tr>
	{if $show_captcha}
    <tr>
		<td>{$astrisk}Enter the characters in the image<br />
	    {$captcha_img}<br />
	    <input onChange="trim_value(this);" type="text" name="captcha_code" id="captcha_code_id" size="21" maxlength="6" />
        <a href="#" onclick="document.getElementById('captcha').src = '{$base_url}lib/securimage/securimage_show.php?' + Math.random(); return false">{$refresh_img}</a>
		</td>
	</tr>
	{/if}
	<tr>
		<td><input type="submit" id="register_id" value="Register" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
