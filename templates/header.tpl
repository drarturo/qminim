<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>{$page_title_sanitize}</title>
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" language="javascript" src="{$base_url}js/overLib/overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script type="text/javascript" language="javascript" src="{$base_url}js/general.js"></script>
{if isset($eula) or isset($supporttypes)}
<script type="text/javascript" language="javascript" src="{$base_url}js/text_area_limiter.js"></script>
{/if}
<link href="{$base_url}templates/styles/style.css" rel="stylesheet" type="text/css" />
</head>
{literal}
<noscript>
    <h1>Your browser does not support javascript!</h1>
    <h1>Please enable javascript in your browser and try again.<h1>
    <style type="text/css">
        #wrap {display:none;}
    </style>
</noscript>
{/literal}
<body onload="focus_first_box();">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<div id="wrap">
<div id="header">
    <div id="header_left">
        <div id="header_right">
        {$addthiscode}
        <h1>{$page_title}<span class="subtitle">{$page_sub_title}</span><img id="logo"  src="{$base_url}templates/images/logo.gif" /></h1>
<div id="screenwide">{if isset($user)}{submit_link action=wide_screen_toggle src=$ws_img subpage=$subpage}{/if}</div>
        </div>
    </div>
</div><!-- div header end -->
<div id="content">
{if isset($user)}
{include file='right_menu.tpl'}
{include file='nav_menu.tpl'}
{/if}
