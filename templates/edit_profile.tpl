{*
 * edit_profile.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{/if}
<form action="index.php" method="post" name="profile_form" onsubmit="return valid_profile_form();">
{csrf}
<input type="hidden" name="action" value="profile_edited">
<div align="center" class="register">
<table>
	<tr>
		<td>Title:{$astrisk}<br />
		{html_options name=salutation id=salutation_id options=$salutations selected=$post.salutation}
	</tr>
	<tr>
		<td>Job Title:{$astrisk}<br />
		{html_options name=job_title id=job_title_id options=$job_titles selected=$post.job_title}
	</tr>
	<tr>
		<td>First Name:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="first_name" id="first_name_id" value="{$post.first_name}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Last Name:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="last_name" id="last_name_id" value="{$post.last_name}" size="39" />
		</td>
	</tr>
	<tr>
		<td>City:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="city" id="city_id" value="{$post.city}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Country:{$astrisk}<br />
		{html_options name=country id=country_id options=$countries selected=$post.country}
		</td>
	</tr>
	<tr>
		<td>Affiliation:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="affiliation" id="affiliation_id" value="{$post.affiliation}" size="39" />
		</td>
	</tr>
	<tr>
		<td><input type="submit" id="register_id" value="Save" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
