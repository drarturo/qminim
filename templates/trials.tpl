{*
 * trials.tpl
 *}
<table class="listing">
<tr class="heading">
<td colspan="3" width="40px" align="center">
{if $max_allowed_trials == 0 || count($trials) < $max_allowed_trials}
{submit_link action=addtrial db_file=$new_trial_file src=$add_img}
{submit_link action=uploadnew db_file=$new_trial_file src=$upload_img}
{/if}
<td>Trial Title</td><td>Trial File</td>
</tr>
{foreach from=$trials key=filename item=title name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align="center" class="action">
    {if $smarty.foreach.forloop.index == $cur_trial}
        {$selected_img}</td><td>
        {submit_link action=showdownload src=$download_img}
    {else}
        {submit_link action=selecttrial db_file=$filename src=$select_img}</td><td>
        {submit_link action=selectndownload db_file=$filename src=$download_img}
    {/if}
    </td>
    <td align="center" class="action">
    {if $smarty.foreach.forloop.index == $cur_trial}
        {$deld_img}
    {else}
        {submit_link action=deletetrial db_file=$filename src=$del_img confirm_msg="Are you sure deleting this trial?"}
    {/if}
    </td>
    <td>{$title}</td><td>{$filename}</td></tr>
{/foreach}
</table>
