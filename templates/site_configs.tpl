{*
 * site_configs.tpl
 *}
{include file='header.tpl'}
{include file='admin_menu.tpl'}
<h3>Site configs</h3>
<table class="listing">
<tr valign='top' class="heading">
<td align='center'>Config name</td><td align='center'>Config value</td>
</tr>
<tr valign="top" class="even">
<td>Files dir</td><td>{$files_dir}</td>
</tr>
<tr valign="top" class="odd">
<td>Settings DB file</td><td>{$settings_db_file}</td>
</tr>
<tr valign="top" class="even">
<td>Users DB file</td><td>{$users_db_file}</td>
</tr>
<tr valign="top" class="odd">
<td>Cache dir</td><td>{$cache_dir}</td>
</tr>
<tr valign="top" class="even">
<td>Template dir</td><td>{$template_dir}</td>
</tr>
<tr valign="top" class="odd">
<td>Swift dir</td><td>{$swift_dir}</td>
</tr>
<tr valign="top" class="even">
<td>Smarty dir</td><td>{$smarty_dir}</td>
</tr>
<tr valign="top" class="odd">
<td>Sqlite version</td><td>{$sqlite_version}</td>
</tr>
</table>
{include file='footer.tpl'}
