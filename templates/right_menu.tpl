{*
 * right_menu.tpl
 *}

<div id="rightbar">
<ul class="rightmenu">
<li>{submit_link action=showhelp alt=show label=Help tag=a}</li>
{if isset($demo)}
    <li>SUPPORT</li>
{else}
    <li>{submit_link action=showsupport label=Support alt=show tag=a}</li>
{/if}
{if isset($demo)}
    <li>demo</li>
{else}
    <li>{submit_link action=index_page currenttemplate=$subpage subpage="user.tpl" label=$user.user_name tag=a same_case=yes}</li>
{/if}
{if isset($user)}
{if isset($loginas)}
<li>{submit_link action=logoutas alt="log" label=Logout tag=a}</li>
{else}
<li>{submit_link action=logout alt="log" label=Logout tag=a}</li>
{/if}
{/if}
{if isset($user.is_admin)}
<li>{submit_link action=admin_panel alt="admin" label=Admin tag=a}</li>
{/if}
</ul>
</div>
