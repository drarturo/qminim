{*
 * support_sent.tpl
 *}
{include file='header.tpl'}
<h3 class="warning">Thank you for sending message. Your message will be considered by the site admin, and you will be informed of the result within 2-3 working days!</h3>
<p align="center">
{submit_link action=index_page label=Return}
</p>
{include file='footer.tpl'}
