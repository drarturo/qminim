{include file='jsform.tpl'}
</div><!-- div content end -->
</div><!-- div wrap end -->
<div id="footer">
    <div id="footer_left">
        <div id="footer_right">
            <div class="foottext">
                Powered by<a href="http://sourceforge.net/projects/qminim/" height="31">QMinim</a>, Copyright © 2013 Mahmoud Saghaei
                {if $show_footer_external_link}
                <a href="http://www.php.net/"><img src="templates/images/php.gif" height="31"></a>
                <a href="http://www.sqlite.org/"><img src="templates/images/sqlite.gif" height="31"></a>
                <a href="http://www.smarty.net/"><img src="templates/images/smarty.gif" height="31"></a>
                <a href="http://www.bosrup.com/web/overlib/"><img src="templates/images/overlib.gif" height="31" alt="Popups by overLIB!" border="0" /></a>
                <a href="http://phpcaptcha.org/" target="_blank"><img src="templates/images/imgcap.gif" height="31" /></a>
                {/if}
                {$followus}
                <br />&nbsp;
            </div><!-- div foottext end -->                
        </div>
    </div>
</div><!-- div footer end -->
{if $set_width}
<script type="text/javascript" language="javascript">
    set_body_width();
    {if isset($scroll_to_end)}
        window.scrollTo(0, document.body.scrollHeight);
    {elseif isset($xpos) && isset($ypos)}
        window.scrollTo({$xpos}, {$ypos});
    {/if}
</script>
{/if}
</body>
</html>
