{*
 * reset_password_form.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{/if}
<h3>Password reset form</h3>
<form action="index.php" method="post" name="reset_password_form" onsubmit="return valid_reset_password_form();">
{csrf}
<input type="hidden" name="action" value="password_reset">
Enter your new password in the following boxes to reset it.
<div align="center" class="register">
<table>
    <tr>
        <td>User name:<br />
        <input readonly type="text" name="user_name" value="{$user_name}" /></td>
    </tr>
	<tr>
		<td>New password:{$astrisk}<br />
		<input onChange="trim_value(this);" type="password" name="new_password" id="new_password_id" value="" size="39" />
		<input type="button" value="Random Password" onclick="insert_random_password('new_password_id', 'new_password_confirm_id', {$rand_pw_len}, 1);" />
		</td>
	</tr>
	<tr>
		<td>Confirm:{$astrisk}<br />
		<input onChange="trim_value(this);" type="password" name="new_password_confirm" id="new_password_confirm_id" value="" size="39" />
		</td>
	</tr>
	<tr>
		<td><input type="submit" value="Save" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
