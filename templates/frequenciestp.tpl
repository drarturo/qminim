{*
 * frequenciestp.tpl
 *}
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th colspan='2' align='center' valign='middle'>
{$frequencies_tt|tts:""}
 {submit_link action=index_page subpage="frequencies.tpl" frequenciestype="frequenciesdr.tpl" alt=transpose src=$transposedr_img}
  {if $all_subjects_count > 0}
    {literal}
    <input type='image' align='bottom' src='templates/images/save_as_preload.gif' height="16" onclick="submit_form('save_as_preload', {subpage:'preload.tpl',preloadtype:'preloaddr.tpl'}, 'Save this frequencies table as preload and clear subjects? All subjects will be deleted!');" alt='Save subjects as preload' title='Save subjects as preload' />
    {/literal}
  {/if}
  <br />
  {if $include_preload}
  {assign var='ip' value='Exclude Preload'}
  {else}
  {assign var='ip' value='Include Preload'}
  {/if}
  {submit_link action=toggle_include_preload subpage="frequencies.tpl" frequenciestype="frequenciestp.tpl" tag=a label=$ip alt=$ip same_case=yes}
</th>
{foreach from=$treatments key=treatment item=r}
    <th align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="$treatment" selected_factor_freq="" selected_level_freq="" label=$treatment tag=a alt=$treatment same_case=yes}</th>
{/foreach}
<th align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=Total tag=a alt=Total same_case=yes}</th>
</tr>
{foreach from=$frequenciestp key=factor item=levels name=forloop}
    <tr>
    {if $smarty.foreach.forloop.last}
        {arr_sum|assign:s arr=$levels}
        <th colspan='2' align='left'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=Total tag=a alt=Total same_case=yes}</th>
        {foreach from=$levels key=treatment item=p}
            <td align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="$treatment" selected_factor_freq="" selected_level_freq="" label=$p tag=a alt=$p same_case=yes}</td>
        {/foreach}
        <td align="center">{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=$s tag=a alt=$s same_case=yes}</td>
    {else}
        {array_count|assign:rs arr=$levels}
        <th rowspan='{$rs}' align='left'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=$factor tag=a alt=$factor same_case=yes}</th>
        {foreach from=$levels key=level item=treatmentsitem name=fl}
            {if ! $smarty.foreach.fl.first}<tr>{/if}
            <th align='left'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="$factor" selected_level_freq="$level" label=$level tag=a alt=$level same_case=yes}</th>
            {foreach from=$treatmentsitem key=treatment item=p}
                <td align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="$treatment" selected_factor_freq="$factor" selected_level_freq="$level" label=$p tag=a alt=$p}</td>
            {/foreach}
            {arr_sum|assign:s arr=$treatmentsitem}
            <td align="center">{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="$factor" selected_level_freq="$level" label=$s tag=a alt=$s same_case=yes}</td>
            </tr>
        {/foreach}
    {/if}
    </tr>
{/foreach}
</table>
