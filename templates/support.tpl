{*
 * support.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{else}
    <div class="warning">Use this form to ask questions, request help, report a bug or give comments or proposal.</div>
{/if}
<form action="index.php" method="post" name="support_form" onsubmit="return valid_support_form();">
{csrf}
<input type="hidden" name="action" value="support_data">
<div align="center" class="register">
<table>
	<tr>
		<td>Category:<br />
		{html_options name=supporttype id=supporttype_id options=$supporttypes selected=$post.supporttype}
	</tr>
	<tr>
		<td>Subject:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="subject_text" id="subject_text_id" value="{$post.subject_text}" size="69" />
		</td>
	</tr>
	<tr>
		<td>{$astrisk}Message: <input readonly type="text" name="CharsLeft" id="CharsLeft_id" size="1">characters remained!<br />
        <textarea cols="80" rows="8" id="comment_id" name="comment"
            onBlur="InputLengthCheck({$max_comment_chars});"
            onKeyUp="InputLengthCheck({$max_comment_chars});">{$post.comment}</textarea><br />
		</td>
	</tr>
	<tr>
		<td><input type="submit" id="support_id" value="Submit" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
