{*
 * settings.tpl
 *}
<table class='listing'>
<tr class='heading'>
<td width='40px' align='center'>
{submit_link action=editsettings src=$edit_img}</td>
<td>Setting Name</td><td>Setting Value</td></tr>
<tr valign='top' class='odd'>
<td rowspan='10' align='center' valign='middle'>
<a href=""><img src='templates/images/{$status|lower}.gif' alt="{$status}" title="{$status}" onclick="return false;" /></a>
{$status_tt|tts:""}
</td><td>Trial Title</td><td>{$title}</td></tr>
<tr valign='top' class='even'>
<td>Probability Method {$prob_method_tt|tts:""}</td><td>{$prob_method}</td></tr>
<tr valign='top' class='odd'>
<td>Base Probability {$base_prob_tt|tts:""}</td><td>{$base_prob}</td></tr>
<tr valign='top' class='even'>
<td>Distance Method {$dist_method_tt|tts:""}</td><td>{$dist_method}</td></tr>
<tr valign='top' class='odd'>
<td>Sample Size {$sample_size_tt|tts:""}</td><td>{$sample_size}</td></tr>
<tr valign='top' class='even'>
<td>Identifier Type {$id_type_tt|tts:""}</td><td>{$id_type}</td></tr>
<tr valign='top' class='odd'>
<td>Identifier Order {$id_order_tt|tts:""}</td><td>{$id_order}</td></tr>
<tr valign='top' class='even'>
<td>Recycle deleted IDs {$recycle_ids_tt|tts:""}</td><td>{if $recycle_ids}Yes{else}No{/if}</td></tr>
<tr valign='top' class='odd'>
<td>Date Initiated</td><td>{$date_initiated}</td></tr>
<tr valign='top' class='even'>
<td>Last Modified</td><td>{$date_modified}</td></tr>
</table>
{if $sample_size < $max_allowed_subjects}
{if $status == 'Enroll' || $status == 'Finished'}
<form action='index.php' method='post' onsubmit="return confirm('Subjects IDs will be left padded to adopt the new sample size!');">
{csrf}<input type="hidden" name="action" value="newsamplesize" />
New sample size: <input type="text" name="new_sample_size" id="new_sample_size_id" value="{if isset($new_sample_size)}{$new_sample_size}{else}{$max_allowed_subjects}{/if}" size="1"><input type="submit" name="submit" value="Set" class="defaultButton" />
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
</form>
{/if}
{/if}
