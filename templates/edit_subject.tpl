{*
 * edit_subject.tpl
 *}
 
{include file='header.tpl'}
<div align="center">
<h2>Edit Subject: <b>{$id}</b></h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<form action='index.php' method='post'>
{csrf}
<input type="hidden" name="action" value="subjectedited" />
<input type="hidden" name="currentpage" value="{$currentpage}" />
<input type="hidden" name="id" value="{$id}" />
<table>
<tr><th align="left">Treatment: </th><td>{html_options name=treatment options=$treatments_arr selected=$treatment}</td></tr>
{foreach from=$factors key=factor item=v}
    <tr><th align="left">{$factor}: </th><td>{html_options name=$factor options=$options_arr[$factor] selected=$selects_arr[$factor]}</td></tr>
{/foreach}
<tr>
<td><input type="submit" name="submit" value="Save" class="defaultButton" /></td>
<td>{submit_link action=index_page label=Return subpage="subjects.tpl"}</td>
</tr>
</table>
</form>
{include file='footer.tpl'}
