{*
 * edit_preloadtp.tpl
 *}
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th colspan='2' align='center' valign='middle'>
&nbsp;
</th>
{foreach from=$treatments key=treatment item=r}
    <th align='center'>{$treatment}</th>
{/foreach}
<th align='center'>Total</th>
</tr>
{foreach from=$preloadtp key=factor item=levels name=forloop}
    <tr>
    {if $smarty.foreach.forloop.last}
        {arr_sum|assign:s arr=$levels}
        <th colspan='2' align='left'>Total</th>
        {foreach from=$levels key=treatment item=p}
            <td align='center'><input type='text' readonly="readonly" name='{$treatment}_total' id='{$treatment}_total_id' value='{$p}' size='1' /></td>
        {/foreach}
        <td align="center"><input type='text' readonly="readonly" id='total_total' value='{$s}' size='1' /></td>
    {else}
        {array_count|assign:rs arr=$levels}
        <th rowspan='{$rs}' align='left'>{$factor}</th>
        {foreach from=$levels key=level item=treatmentsitem name=fl}
            {if ! $smarty.foreach.fl.first}<tr>{/if}
            <th align='left'>{$level}</th>
            {foreach from=$treatmentsitem key=treatment item=p}
                <td align='center'>
                <input onChange="trim_value(this);" type='text' name='{$treatment}_{$factor}_{$level}' value='{$p}' size='1' onblur="update_preload(this);" />
                </td>
            {/foreach}
            {arr_sum|assign:s arr=$treatmentsitem}
            <td align="center">
            <input type='text' readonly="readonly" id='total_{$factor}-{$level}' value='{$s}' size='1' />
            </td>
            </tr>
        {/foreach}
    {/if}
    </tr>
{/foreach}
</table>
