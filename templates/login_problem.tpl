{*
 * login_problem.tpl
 *}
{include file='header.tpl'}
<div class="warning">Instructions:
    <ul>
        <li>Step 1. Enter your username or email. The system will send a code to your email if it found your name.</li>
        <li>Step 2. Copy the code from your mail box and enter it in the second box. If necessary open this page again.</li>
        <li>Step 3. Your username will be presented to you and you will be prompted to enter you new password.</li>
        <li>Step 4. Log in into the system, using your new password. Keep your new password in a safe place.</li>
    </ul>
</div>
{if isset($err_msg)}
    {$err_msg}
{/if}
<form action="index.php" method="post" name="login_problem_form" onsubmit="return valid_login_problem_form();">
{csrf}
<input type="hidden" name="action" value="login_problem_data">
<div align="center" class="register">
<table>
	<tr>
		<td>Username or email:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="user_name_or_email" id="user_name_or_email_id" value="{if isset($post) and isset($post.user_name_or_email)}{$post.user_name_or_email}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Code:<br />
		<input onChange="trim_value(this);" type="text" name="reset_code" id="reset_code_id" value="{if isset($post) and isset($post.reset_code)}{$post.reset_code}{/if}" size="39" />
		</td>
	</tr>
    <tr>
		<td>{$astrisk}Enter the characters in the image<br />
	    {$captcha_img}<br />
	    <input onChange="trim_value(this);" type="text" name="captcha_code" id="captcha_code_id" size="21" maxlength="6" />
        <a href="#" onclick="document.getElementById('captcha').src = '{$base_url}lib/securimage/securimage_show.php?' + Math.random(); return false">{$refresh_img}</a>
		</td>
	</tr>
	<tr>
		<td><input type="submit" id="login_problem_id" value="Submit" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
