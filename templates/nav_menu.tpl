{*
 * nav_menu.tpl
 *}

<div id='navbar'>
<ul class='menu'>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="trials.tpl" label=Trials tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="treatments.tpl" label=Treatments tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="factors.tpl" label=Factors tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="settings.tpl" label=Settings tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="preload.tpl" label=Preload tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="subjects.tpl" label=Subjects tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="frequencies.tpl" label=Frequencies tag=a}</li>
<li>{submit_link action=index_page currenttemplate=$subpage subpage="balance.tpl" label=Balance tag=a}</li>
<li>{submit_link action=showdownload alt="show" label=Backup tag=a}</li>
{if isset($demo)}
<li>RESTORE</li>
{else}
<li>{submit_link action=uploadtrial label="Restore" tag=a alt=upload confirm_msg="Are you sure uploading a new trial? Current trial will be lost!"}</li>
{/if}
<li>{submit_link action=resettrial alt="reset" label=New tag=a confirm_msg="Are you sure? All trial data including subjects will be lost!"}</li>
<li>{submit_link action=trialsummary alt="summary" label=Summary tag=a}</li>
</ul>
</div>
