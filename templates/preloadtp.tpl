<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th colspan='2' align='center' valign='middle'>
{$preload_tt|tts:""}
{submit_link action=editpreload preloadtype="preloadtp.tpl" src=$edit_img}
{submit_link action=clearpreload preloadtype="preloadtp.tpl" src=$clear_img confirm_msg="Clear preload?"}
{submit_link action=index_page subpage="preload.tpl" preloadtype="preloaddr.tpl" alt=transpose src=$transposedr_img}
</th>
{foreach from=$treatments key=treatment item=r}
    <th align='center'>{$treatment}</th>
{/foreach}
<th align='center'>Total</th>
</tr>
{foreach from=$preloadtp key=factor item=levels name=forloop}
    <tr>
    {if $smarty.foreach.forloop.last}
        {arr_sum|assign:s arr=$levels}
        <th colspan='2' align='left'>Total</th>
        {foreach from=$levels item=p}
            <td align='center'>{$p}</td>
        {/foreach}
        <td align="center">{$s}</td>
    {else}
        {array_count|assign:rs arr=$levels}
        <th rowspan='{$rs}' align='left'>{$factor}</th>
        {foreach from=$levels key=level item=treatmentsitem name=fl}
            {if ! $smarty.foreach.fl.first}<tr>{/if}
            <th align='left'>{$level}</th>
            {foreach from=$treatmentsitem item=p}
                <td align='center'>{$p}</td>
            {/foreach}
            {arr_sum|assign:s arr=$treatmentsitem}
            <td align="center">{$s}</td>
            </tr>
        {/foreach}
    {/if}
    </tr>
{/foreach}
</table>
