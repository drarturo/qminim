{*
 * send_message.tpl
 *}
<form action='index.php' method='post' onsubmit="return valid_send_message_form();">
{csrf}
<input type="hidden" name="action" value="message_sent" />
<input type="hidden" name="user_email" value="{$post.user_email}" />
<div align="center">
<h2>Send Message to {$post.user_email}</h2>
{if isset($err_msg)}
    {$err_msg}
{/if}
<table>
    <tr>
        <td>
            Subject<br />
            <input onChange="trim_value(this);" id="message_subject_id" type='text' name='message_subject' size='39' value='{if isset($post) and isset($post.message_subject)}{$post.message_subject}{/if}'>
        </td>
    </tr>
	<tr>
		<td>
		    Message:<br />
            <textarea cols="80" rows="8" id="message_body_id" name="message_body">{if isset($post) and isset($post.message_body)}{$post.message_body}{/if}</textarea><br />
		</td>
	</tr>
    <tr>
        <td>
            <input type="submit" name="submit" value="Send" class="defaultButton" />
            {submit_link action=site_users label=Return}
        </td>
    </tr>
</table>
</div>
</form>
