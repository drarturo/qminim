{*
 * activation_code.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}<br />
{/if}
<div class="warning">Thank you for your request. An activation code was sent to your email. Enter the activation code below to complete your registration!</div>
<form action="index.php" method="post" name="activation_code_form" onsubmit="return valid_activation_code_form();">
{csrf}
<input type="hidden" name="action" id="action_id" value="activation_code_data">
<div align="center" class="register">
<table>
	<tr>
		<td>Email:
        <input onChange="trim_value(this);" type="text" name="email" id="email_id" value="{if isset($post)}{$post.email}{/if}" size="39" />
	    </td>
	</tr>
	<tr>
		<td>Activation code:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="activation_code" id="activation_code_id" value="{if isset($post)}{if isset($post.activation_code)}{$post.activation_code}{/if}{/if}" size="39" />
		</td>
	</tr>
	{if $show_captcha}
    <tr>
		<td>{$astrisk}Enter the characters in the image<br />
	    {$captcha_img}<br />
	    <input onChange="trim_value(this);" type="text" name="captcha_code" id="captcha_code_id" size="21" maxlength="6" />
        <a href="#" onclick="document.getElementById('captcha').src = '{$base_url}lib/securimage/securimage_show.php?' + Math.random(); return false">{$refresh_img}</a>
		</td>
	</tr>
	{/if}
	<tr>
		<td><input type="submit" id="pre_register_id" value="Submit" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
