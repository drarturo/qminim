{*
 * add_user.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{/if}
<form action='index.php' method='post' onsubmit="return valid_add_user_form({$rand_pw_len});">
{csrf}
<input type="hidden" name="action" value="useradded" />
<input type="hidden" name="user_source" value="{$post.user_source}" />
<div align="center">
<h2>Add User</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<table>
	<tr>
		<td>Title:{$astrisk}<br />
		{html_options name=salutation id=salutation_id options=$salutations selected=$post.salutation}
	</tr>
	<tr>
		<td>Job Title:{$astrisk}<br />
		{html_options name=job_title id=job_title_id options=$job_titles selected=$post.job_title}
	</tr>
	<tr>
		<td>First Name:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="first_name" id="first_name_id" value="{$post.first_name}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Last Name:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="last_name" id="last_name_id" value="{$post.last_name}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Email:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="email" id="email_id" value="{$post.email}" size="39"{if $post.user_source == 'requested'}readonly{/if} />
		</td>
	</tr>
	<tr>
		<td>City:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="city" id="city_id" value="{$post.city}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Country:{$astrisk}<br />
		{html_options name=country id=country_id options=$countries selected=$post.country}
		</td>
	</tr>
	<tr>
		<td>Affiliation:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="affiliation" id="affiliation_id" value="{$post.affiliation}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Username:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="user_name" id="user_name_id" value="{$post.user_name}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Password:{$astrisk}<input onChange="trim_value(this);" type="button" value="Random Password" onclick="insert_random_password('password_id', 'password_confirm_id', {$rand_pw_len}, 0);" /><br />
		<input onChange="trim_value(this);" type="password" name="password" id="password_id" value="{if isset($post) and isset($post.password)}{$post.password}{/if}" size="39" />
		</td>
	</tr>
	<tr>
		<td>Confirm:{$astrisk}<br />
		<input onChange="trim_value(this);" type="password" name="password_confirm" id="password_confirm_id" value="{if isset($post) and isset($post.password_confirm)}{$post.password_confirm}{/if}" size="39" />
		</td>
	</tr>
<tr>
<td><input type="checkbox" name="show_email_form" value="show_email_form" />Show email form&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Add user" class="defaultButton" />{submit_link action=site_users label=Return}</td>
</tr>
</table>
</div>
</form>
{include file='footer.tpl'}
