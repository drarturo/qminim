{*
 * error_logs.tpl
 *}
{include file='header.tpl'}
{include file='admin_menu.tpl'}
<h2>QMinim: Error logs </h2>
{submit_link action=clear_error_logs label="Clear logs" tag=a}
<pre>
{php}
$logs = htmlentities(file_get_contents(FILES_DIR . 'error.log'));
if ($logs) {
    echo $logs;
} else {
    echo 'No log entry!';
}
{/php}
</pre>
{include file='footer.tpl'}
