{*
 * pre_registered.tpl
 *}
{include file='header.tpl'}
{if isset($err_msg)}
    {$err_msg}
{else}    
    <div class="warning">Thank you for your request. An activation code was sent to your email. Enter the activation code below to complete your registration!</div>
{/if}
<form action="index.php" method="post" name="pre_registered_form" onsubmit="return valid_pre_registered_form();">
{csrf}
<input type="hidden" name="action" value="pre_register_data">
<div align="center" class="register">
<table>
	<tr>
		<td>Email:{$astrisk}<br />
		<input onChange="trim_value(this);" type="text" name="email" id="email_id" value="{$post.email}" size="39" />
		</td>
	</tr>
    <tr>
		<td>{$astrisk}Enter the characters in the image<br />
	    {$captcha_img}<br />
	    <input onChange="trim_value(this);" type="text" name="captcha_code" id="captcha_code_id" size="21" maxlength="6" />
        <a href="#" onclick="document.getElementById('captcha').src = '{$base_url}lib/securimage/securimage_show.php?' + Math.random(); return false">{$refresh_img}</a>
		</td>
	</tr>
	<tr>
		<td><input type="submit" id="pre_register_id" value="Submit" />
	    <input type="reset" />
		</td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
