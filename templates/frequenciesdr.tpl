{*
 * frequenciesdr.tpl
 *}
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
{$frequencies_tt|tts:""}
  {submit_link action=index_page subpage="frequencies.tpl" frequenciestype="frequenciestp.tpl" alt=transpose src=$transposetp_img}
  {if $all_subjects_count > 0}
    {literal}
    <input type='image' align='bottom' src='templates/images/save_as_preload.gif' height="16" onclick="submit_form('save_as_preload', {subpage:'preload.tpl',preloadtype:'preloaddr.tpl'}, 'Save this frequencies table as preload and clear subjects? All subjects will be deleted!');" alt='Save subjects as preload' title='Save subjects as preload' />
    {/literal}
  {/if}
  <br />
  {if $include_preload}
  {assign var='ip' value='Exclude Preload'}
  {else}
  {assign var='ip' value='Include Preload'}
  {/if}
  {submit_link action=toggle_include_preload subpage="frequencies.tpl" frequenciestype="frequenciesdr.tpl" tag=a label=$ip alt=$ip same_case=yes}
</th>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=$factor tag=a alt=$factor same_case=yes}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=Total tag=a alt=Total same_case=yes}</th>
</tr><tr>
{foreach from=$factors key=factor item=value}
    {foreach from=$value[1] item=level}
        <th align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="$factor" selected_level_freq="$level" label=$level tag=a alt=$level same_case=yes}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$frequencies key=treatment item=factors_ext}
    <tr>
    <th align='left'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="$treatment" selected_factor_freq="" selected_level_freq="" label=$treatment tag=a alt=$treatment same_case=yes}</th>
    {foreach from=$factors_ext key=factor item=levels_ext}
        {arr_sum|assign:s arr=$levels_ext}
        {foreach from=$levels_ext key=level_name item=level_value}
            <td align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="$treatment" selected_factor_freq="$factor" selected_level_freq="$level_name" label=$level_value tag=a alt=$level_value}</td>
        {/foreach}
    {/foreach}
    <td align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="$treatment" selected_factor_freq="" selected_level_freq="" label=$s tag=a alt=$s}</td>
    </tr>
{/foreach}
<tr><th align='left'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=Total tag=a alt=Total same_case=yes}</th>
{foreach from=$factors key=factor item=value}
    {foreach from=$value[1] item=level}
        {assign var=fl value="$factor-$level"}
    <td align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="$factor" selected_level_freq="$level" label=$fgrand_total[$fl] tag=a alt=$fgrand_total[$fl]}</td>
    {/foreach}
{/foreach}
<td align='center'>{submit_link action=set_subjects_filter_from_freq subpage="subjects.tpl" selected_treatment_freq="" selected_factor_freq="" selected_level_freq="" label=$fgrand_total.total tag=a alt=$fgrand_total.total}</td>
</tr></table>
