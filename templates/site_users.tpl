{*
 * site_users.tpl
 *}
{include file='header.tpl'}
{include file='admin_menu.tpl'}
{array_count|assign:uc arr=$users}
<h3>Current users ({$uc})</h3>
<table class="listing">
<tr valign='top' class="heading">
<td colspan='4' width='60px' align='center'>
{submit_link action=adduser src=$add_img}
</td>
<td>User name</td>
<td>First name</td>
<td>Last name</td>
<td>Email</td>
<td colspan="4" width='60px' align='center'>{submit_link action=sendmsg user_name=all user_email=all src=$msg_img}</td>
</tr>
{foreach from=$users item=site_user name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align='center'><a name="{$site_user.user_name}" />
    {if !isset($site_user.is_admin)}
    {if $site_user.disabled}
        {assign var='status_img' value=$disabled_img}
        {assign var='action' value="enable user"}
        {assign var='alt' value=enable}
        {submit_link action=$action user_name=$site_user.user_name user_email=$site_user.email src=$status_img alt=$alt hash=$site_user.user_name}
    {else}
        {assign var='status_img' value=$enabled_img}
        {assign var='action' value="disable user"}
        {assign var='alt' value=disable}
        {submit_link action=$action user_name=$site_user.user_name user_email=$site_user.email src=$status_img alt=$alt prompt_msg="disabled_reason:Enter disabled reason:Your account is disabled" hash=$site_user.user_name}
    {/if}
    {/if}
    </td>
    <td align='center'>
    {submit_link action=edituser user_name=$site_user.user_name src=$edit_img}
    </td>
    <td align='center'>
    {if !isset($site_user.is_admin)}
    {submit_link action=deleteuser user_name=$site_user.user_name src=$del_img confirm_msg="Are you sure you want to delete this user?"}
    {/if}
    </td>
    <td>
    {ups user=$site_user}</td>
    <td>
    {assign var='site_user_name' value=$site_user.user_name}
    {submit_link action=changeusername user_name=$site_user.user_name email=$site_user.email label=$site_user.user_name tag=a same_case=yes prompt_msg="new_user_name:Enter new username:$site_user_name"}{if isset($changing_user_name) && $changing_user_name eq $site_user.user_name && isset($err_msg)}{$err_msg}{/if}
    </td>
    <td>{$site_user.first_name}</td>
    <td>{$site_user.last_name}</td>
    <td>{$site_user.email}</td>
    <td>
    {if isset($site_user.is_admin)}
        {submit_link action=admintoggle user_name=$site_user.user_name src=$admin_img label=A alt='toggle' tag=a}
    {else}
        {if !$site_user.disabled && isset($can_toggle)}
            {submit_link action=admintoggle user_name=$site_user.user_name src=$noadmin_img label=N alt='toggle' tag=a}
        {else}
            N
        {/if}
    {/if}
    </td>
    <td>
    {if !$site_user.disabled}
        {submit_link action=loginas user_name=$site_user.user_name src=$login_as_img}
    {/if}
    </td>
    <td>
    {submit_link action=sendmsg user_name=$site_user.user_name user_email=$site_user.email src=$msg_img}
    </td>
    <td>
    {submit_link action=repairpassword user_name=$site_user.user_name src=$repair_img prompt_msg="password:Enter password for this user:"}
    </td>
    </tr>
{/foreach}
</table>
{if $step_users}
<hr />
<h3>Pending emails</h3>
<table class="listing">
<tr valign='top' class="heading">
<td colspan='2' width='60px' align='center'>
</td>
<td>Email</td>
<td>Number of requests</td>
<td>Last request</td>
<td>Activation code</td>
</tr>
{foreach from=$step_users item=site_user name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align='center'>
    {submit_link action=adduser email=$site_user.email src=$edit_img}
    </td>
    <td align='center'>
    {assign var='msg' value="Are you sure you want to delete this request email?"}
    {submit_link action=deleterequestemail email=$site_user.email src=$del_img confirm_msg=$msg}
    </td>
    <td>{$site_user.email}</td>
    <td>{$site_user.num_requests}</td>
    <td>{$site_user.date_requested}</td>
    <td>{$site_user.act_code}</td>
    </tr>
{/foreach}
</table>
{/if}
{include file='footer.tpl'}
