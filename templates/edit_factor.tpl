{*
 * edit_factor.tpl
 *}
{include file='header.tpl'}
<form action='index.php' method='post' onsubmit="return valid_factor_form({$max_allowed_levels}, {$max_allowed_chars});">
{csrf}
<input type="hidden" name="action" value="factoredited" />
<input type="hidden" name="initialfactor" value="{$initialfactor}" />
<input type="hidden" name="levels" id="levels_str_id" value="" />
<div align="center">
<h2>Edit Factor</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<table>
<tr>
<td>{$valididentifier_tt|tts|replace:'%s':$max_allowed_chars|replace:'%id_name':factor}Factor Name</td>
<td><input onChange="trim_value(this);" id="factor_name_id" maxlength="{$max_allowed_chars}" type='text' name='factor' size='20' value='{$factor}'></td>
</tr>
<tr>
<td>{$factorweight_tt|tts}Factor Weight</td>
<td><input onChange="trim_value(this);" id="factor_weight_id" type='text' name='weight' size='6' value='{$weight}'></td>
</tr>
<tr>
<td>{$validlevelidentifier_tt|tts|replace:'%s':$max_allowed_chars}Enter levels:</td>
<td><input onChange="trim_value(this);" type="text" id="temp_level_id" value="" maxlength="{$max_allowed_chars}"{if $status == 'Setup'} onkeypress="return levelPressed(this, event, {$max_allowed_levels});"{/if} />(Max = {$max_allowed_levels})</td>
</tr>
<tr>
<td align="right">
<input type="button" value="Add" style="width: 90px" onclick="levelAddClicked({$max_allowed_levels});"{if $status != 'Setup'}  disabled="disabled"{/if} />
</td>
<td  rowspan="4" style="border: solid gray 1px;">
    <div class="bloc">
        <select id="levels_id" size="{$max_allowed_levels}" onclick="setLevelBoxText();" onkeypress="setLevelBoxText();">
            {html_options values=$levels output=$levels}
        </select>
    </div>
</td>
</tr>
<tr>
    <td align="right"><input type="button" value="Categorize" style="width: 90px" onclick="levelCatClicked({$max_allowed_levels});"{if $status != 'Setup'} disabled="disabled"{/if} /></td>
</tr>
<tr>
    <td align="right"><input type="button" value="Delete" style="width: 90px" onclick="levelDeleteClicked();"{if $status != 'Setup'}  disabled="disabled"{/if} /></td>
</tr>
<tr>
    <td align="right"><input type="button" value="Change" style="width: 90px" onclick="levelChangeClicked();" /></td>
</tr>
<tr>
<td><input type="submit" name="submit" value="Save" class="defaultButton" /></td>
<td>{submit_link action=index_page label=Return subpage="factors.tpl"}</td>
</tr>
</table>
</div>
</form>
{include file='footer.tpl'}
