{*
 * edit_settings.tpl
 *}
{include file='header.tpl'}
<form action='index.php' method='post' onsubmit="return valid_settings_form({$max_allowed_subjects});">
{csrf}
<input type="hidden" name="action" value="settingsedited" />
<div align="center">
<h2>Edit Settings</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<table>
<tr><th align="left">Trial Title:</th><td><input onChange="trim_value(this);" id="title_id" type='text' name='title' value='{$title}' /></td></tr>
<tr><th align="left">{$prob_method_tt|tts:""}Probability method:</th><td>
{html_options name=prob_method options=$prob_arr selected=$prob_method}
</td></tr>
<tr><th align="left">{$base_prob_tt|tts:""}Base probability:</th><td><input onChange="trim_value(this);" type='text' name='base_prob' id='base_prob_id' value='{$base_prob}' /></td></tr>
<tr><th align="left">{$dist_method_tt|tts:""}Distance method:</th><td>
{html_options name=dist_method options=$dist_arr selected=$dist_method}
</td></tr>
<tr><th align="left">{$sample_size_tt|tts:""}Sample size:</th><td><input onChange="trim_value(this);" id="sample_size_id" type='text' name='sample_size' value='{$sample_size}' /></td></tr>
<tr><th align="left">{$id_type_tt|tts:""}Identifier type:</th><td>
{html_options name=id_type options=$id_type_arr selected=$id_type}
</td></tr>
<tr><th align="left">{$id_order_tt|tts:""}Identifier order:</th><td>
{html_options name=id_order options=$id_order_arr selected=$id_order}
</td></tr>
<tr><th align="left">{$recycle_ids_check_tt|tts:""}Recycle deleted IDs:</th><td>
<input type='checkbox' name='recycle_ids' value='1'{if $recycle_ids} checked='checked'{/if} />
</td></tr>
<tr>
<td><input type="submit" name="submit" value="Save" class="defaultButton" /></td>
<td>{submit_link action=index_page label=Return subpage="settings.tpl"}</td>
</tr>
</table>
</form>
{include file='footer.tpl'}
