{*
 * reset_password_success.tpl
 *}
{include file='header.tpl'}
<h3>Your password was reset successfully</h3>
Click <b>{submit_link action=login_page label=here tag=a alt=login_page}</b> to login
{include file='footer.tpl'}
