{*
 * test_trial.tpl
 *}
<div align="center">
<h1>Trial uploaded successfully! Please carefully review the uploaded trial summary below and see if it is what you have wanted. To finalized the upload process click "Apply" at the end of this page. To cancel the upload process click "Cancel".</h1>
<hr />
<h2>Uploaded trial summary</h2>
<h3>Treatments</h3>
<table class="listing">
<tr class="heading">
<td>Treatment</td><td width="40px">Ratio</td>
</tr>
{foreach from=$test_treatments key=treatment item=ratio name=forloop}
<tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td>{$treatment}</td><td>{$ratio}</td>
</tr>
{/foreach}
</table>
<h3>Factors</h3>
<table class="listing">
<tr class="heading">
<td>Factor &#123;Levels&#125;</td><td width='40px'>Weight</td>
</tr>
{foreach from=$test_factors key=factor item=value name=forloop}
    {assign var='weight' value=$value[0]}
    {assign var='levels' value=$value[1]}
    {capture name=ls assign=commalevels}{foreach from=$levels item=level name=levelloop}{$level}{if not $smarty.foreach.levelloop.last}, {/if}{/foreach}{/capture}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td>{$factor} &#123;{$commalevels}&#125;</td>
    <td>{$weight}</td></tr>
{/foreach}
</table>
<h3>Settings</h3>
<table class='listing'>
<tr class='heading'>
<td>Setting Name</td><td>Setting Value</td></tr>
<tr valign='top' class='even'>
<td>Probability Method</td><td>{$test_prob_method}</td></tr>
<tr valign='top' class='odd'>
<td>Base Probability</td><td>{$test_base_prob}</td></tr>
<tr valign='top' class='even'>
<td>Distance Method</td><td>{$test_dist_method}</td></tr>
<tr valign='top' class='odd'>
<td>Sample Size</td><td>{$test_sample_size}</td></tr>
<tr valign='top' class='even'>
<td>Identifier Type</td><td>{$test_id_type}</td></tr>
<tr valign='top' class='odd'>
<td>Identifier Order</td><td>{$test_id_order}</td></tr>
<tr valign='top' class='even'>
<td>Date Initiated</td><td>{$test_date_initiated}</td></tr>
</table>
<h3>Preload</h3>
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
&nbsp;</th>
{foreach from=$test_factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{$factor}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>Total</th>
</tr><tr>
{foreach from=$test_factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {foreach from=$levels item=level}
        <th align='center'>{$level}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$test_preload key=treatment item=factors}
    <tr>
    <th align='left'>{$treatment}</th>
    {foreach from=$factors key=factor item=levels}
        {arr_sum|assign:s arr=$levels}
        {foreach from=$levels key=level_name item=level_value}
            <td align='center'>{$level_value}</td>
        {/foreach}
    {/foreach}
    <td align='center'>{$s}</td>
    </tr>
{/foreach}
<tr><th align='left'>Total</th>
{foreach from=$test_pgrand_total item=v}
    <td align='center'>{$v}</td>
{/foreach}
</tr></table>
<h3>Subjects</h3>
<table class='listing'>
<tr valign='top' class='heading'>
<td width='50px'>ID</td>
<td>Treatment</td>
{foreach from=$test_factors key=factor item=v}
    <td>{$factor}</td>
{/foreach}
</tr>
{foreach from=$test_subjects key=id item=subject name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td>{$id}</td>
    <td>{$subject.treatment}</td>
    {foreach from=$test_factors key=factor item=values}
        <td>{$subject.$factor}</td>
    {/foreach}
    </tr>
{/foreach}
</table>
<h3>Frequencies</h3>
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
&nbsp;
</th>
{foreach from=$test_factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{$factor}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>Total</th>
</tr><tr>
{foreach from=$test_factors key=factor item=value}
    {foreach from=$value[1] item=level}
        <th align='center'>{$level}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$test_frequencies key=treatment item=factors}
    <tr>
    <th align='left'>{$treatment}</th>
    {foreach from=$factors key=factor item=levels}
        {arr_sum|assign:s arr=$levels}
        {foreach from=$levels key=level_name item=level_value}
            <td align='center'>{$level_value}</td>
        {/foreach}
    {/foreach}
    <td align='center'>{$s}</td>
    </tr>
{/foreach}
<tr><th align='left'>Total</th>
{foreach from=$test_fgrand_total item=v}
    <td align='center'>{$v}</td>
{/foreach}
</tr></table>
<table>
<tr>
<td>{submit_link action=index_page label=Cancel}&nbsp;
{submit_link action=applytrialupload label=Apply test_trial_folder=$test_trial_folder}</td>
</tr>
</table>
<hr />
</div>
