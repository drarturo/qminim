{*
 * admin_menu.tpl
 *}
 <h2>QMinim: Admin Panel </h2>
<ul class='menu'>
{if ($user.rank > 4)}
    <li>{submit_link action=site_configs alt="configs" label="Site configs" tag=a}</li>
    <li>{submit_link action=site_settings alt="settings" label="Site settings" tag=a}</li>
    <li>{submit_link action=system_information alt="information" label="System information" tag=a}</li>
    <li>{submit_link action=clear_data_cache alt="data cache" label="Clear data cache" tag=a}</li>
    <li>{submit_link action=clear_template_cache alt="template cache" label="Clear template cache" tag=a}</li>
    <li>{submit_link action=show_error_logs alt="logs" label="Error logs" tag=a}</li>
{/if}
<li>{submit_link action=site_users alt="users" label=Users tag=a}</li>
</ul>
<hr />
