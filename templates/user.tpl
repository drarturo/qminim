{*
 * user.tpl
 *}
<table class='listing'>

<tr class="even"><td width="150px" align='left'>Salutation</td>
<td align='left'>{$user.salutation}</td></tr>

<tr class="odd"><td align='left'>First name</td>
<td align='left'>{$user.first_name}</td></tr>

<tr class="even"><td align='left'>Last name</td>
<td align='left'>{$user.last_name}</td></tr>

<tr class="odd"><td align='left'>Job title</td>
<td align='left'>{$user.job_title}</td></tr>

<tr class="even"><td align='left'>Email</td>
<td align='left'>{$user.email}</td></tr>

<tr class="odd"><td align='left'>Username</td>
<td align='left'>{$user.user_name}</td></tr>

<tr class="even"><td align='left'>Affiliation</td>
<td align='left'>{$user.affiliation}</td></tr>

<tr class="odd"><td align='left'>Country</td>
<td align='left'>{$user.country}</td></tr>

<tr class="even"><td align='left'>City</td>
<td align='left'>{$user.city}</td></tr>

<tr class="odd"><td align='left'>Date registered</td>
<td align='left'>{$user.date_registered}</td></tr>

</table>
<ul class='menu'>
<li>{submit_link action=edit_profile alt="Edit profile" label="Edit profile" tag=a}</li>
<li>{submit_link action=change_password alt="Change password" label="Change password" tag=a}</li>
</ul>
