{*
 * upload_trial.tpl
 *}
{include file='header.tpl'}
<form enctype='multipart/form-data' action='index.php' method='POST'>
{csrf}
<input type="hidden" name="action" value="trialuploaded" />
<input type='hidden' name='MAX_FILE_SIZE' value='204800' />
<div align="center">
<h2>Minimization Online: Upload Trial</h2>
{if isset($save_msg)}
    <font color='red'>{$save_msg}</font>
{/if}
<table>
    <tr>
        <td>Trial zip file:<br />
        <input id='upload_id' name='uploadedtrialfile' type='file' /></td>
    </tr>
    <tr>
        <td><input type='submit' value='Upload' />&nbsp;
        {submit_link action=index_page label=Return subpage=$subpage}
        </td>
    </tr>
</table>
</div>
</form>
{include file='footer.tpl'}
