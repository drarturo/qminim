{*
 * login.tpl
 *}
{include file='header.tpl'}
{literal}
<style>
#content {
    background-color: #ffffff;
}
#logintable {
    background-color: #eeeeee;
}
</style>
{/literal}
<table width="100%" align="center">
<tr valign="top">
<td rowspan="2">
{if ! $register_disabled}
<h2>QMinim: Free online minimisation </h2>
<h3>"Anyone who considers arithmetical methods of producing random digits is, of course, in a state of sin" -- John von Neumann 1951</h3>
QMinim is the online version of <a target="_blank" href="http://sourceforge.net/projects/minimpy/">MinimPy</a><sup>1</sup> and is a free minimisation service for minimising subjects to different arms of a clinical trial. To use this service you have to register for an account. Your request will be considered by the site admin and after approval your account will be activated.<br />
<br /><br /><b>Demo acount:</b><br />
username = demo<br />
password = demo<br />
<sup>1</sup>To cite QMinim:
Saghaei, M. and Saghaei, S. (2011) Implementation of an open-source customizable minimization program for allocation of patients to parallel groups in clinical trials. Journal of Biomedical Science and Engineering, 4, 734-739. doi: 10.4236/jbise.2011.411090. 
Available at: <a target="_blank" href="http://www.scirp.org/journal/PaperInformation.aspx?PaperID=8518">http://www.scirp.org/journal/PaperInformation.aspx?PaperID=8518</a><br />
{/if}
<hr />
<table>
    <tr>
        <td rowspan="2">
            <img src="templates/images/minimizer0.jpg" width="135" height="240" />
        </td>
        <td align="left">
            <h2>Minimizer App for Android</h2>
            <iframe width="220" height="95" scrolling="no" frameborder="0" style="margin:0;padding:0" src="http://slideme.org/widgets/app/12532706/mini_light">Get the <a href="http://slideme.org/application/minimizer">Minimizer</a> Android app from <a href="http://slideme.org/">SlideME</a>.</iframe><div style="margin:-5px 0;padding:0;font-size:small">Get the <a href="http://slideme.org/application/minimizer">Minimizer</a> app from <a href="http://slideme.org/">SlideME</a>.</div><br />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="left">
            <img src="templates/images/minimizer1.jpg" width="240" height="135" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
        Minimizer is an android app for minimizing subjects to different arms of a clinical trial. Minimization is the process of allocating a subject to an arm of a clinical trial in a way to minimize the differences between arms with respect to predetermined prognostic factors. It is superior to randomization because it minimize the difference among grous in addition to reducing imbalances. For more information about minimization usage in clinical trials see http://en.wikipedia.org/wiki/Minimisation_(clinical_trials).
To make most of Minimizer you should have general background knowledge regarding minimization usage in clinical trials.
Minimizer app is a menu-based application with the following main features:
1-Minimization Setting: base probability, probabilty/distance methods and a bunch of other customization icluded in the app, all with working defaults.<br />
2-Treatments: for adding/editing treatment arms. For each treatment a unique lable and an allocation ration may be defined.<br />
3-Factors: for adding/editing trial factors. Each factor may have a unique label and some levels together with a factor weight to imply its importance.<br />
4-Subjects: A mega table for frequency distribution of subjects accross all treatments and levels of each factor. The table is directly editable. There is an easy interface containing dropdown lists for selecting level of each factor and to to enroll the new subject using minimization<br />
5-Support for selecting your current trial and import/exporting existing trials.<br />
        </td>
    </tr>
</table>
</td>
<td>
<div align="center" class="login">
<table id="logintable">
	<tr>
		<th colspan="3">{html_image basedir="$base_dir" file="templates/images/l_lock.gif" alt="Login"}</th>
	</tr>
<form id="login_form_id" action="index.php" method="post" onsubmit="return valid_login_form();">
{csrf}
<input type="hidden" name="action" value="login_data">
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td>Username: </td>
		<td>
		<input onChange="trim_value(this);" type="text" name="user_name" id="user_name_id" value="{if isset($user_name)}{$user_name}{/if}" size="14"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td>Password: </td>
		<td>
		<input onChange="trim_value(this);" type="password" name="password" id="password_id" value="" size="14"/>
		</td>
	</tr>
	{if isset($show_captcha)}
    <tr>
    <td>&nbsp;&nbsp;&nbsp;</td>
	<td colspan="2">Enter the characters in the image<br />
    {$captcha_img}<br />
    <input onChange="trim_value(this);" type="text" name="captcha_code" id="captcha_code_id" maxlength="6" size="21"/>
    <a href="#" onclick="document.getElementById('captcha').src = '{$base_url}lib/securimage/securimage_show.php?' + Math.random(); return false">{$refresh_img}</a>
	</td>
	</tr>
	{/if}
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td colspan="2" align="center">
		<input type="submit" value="Login" />&nbsp;<input type="reset" />
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
	    <td colspan="2">
		[{submit_link action=login_problem label="Login problem" same_case=yes tag=a}]
        {if isset($login_error)}
            <font color='red'>Wrong username or password!</font>
		{/if}
        {if isset($security_error)}
            <font color='red'>Wrong security code!</font>
		{/if}
	    </td>
	</tr>
</form>
    {if ! $register_disabled}
    <tr>
    <td colspan="2">{submit_link action=pre_register src=$signup_img}</td>
    </tr>
    {/if}
<tr>
<td>
<iframe width="100%" height="360px" scrolling="no" frameborder="0" style="margin:0;padding:0" src="http://slideme
.org/widgets/app/15048920/banner">Get the <a href="http://slideme.org/application/equationpro">EquationPro</a> An
droid app from <a href="http://slideme.org/">SlideME</a>.</iframe><div style="margin:-5px 0;padding:0;font-size:s
mall">Get the <a href="http://slideme.org/application/equationpro">EquationPro</a> Android app from <a href="http
://slideme.org/">SlideME</a>.</div>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
{include file='footer.tpl'}
