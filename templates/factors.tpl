{*
 * factors.tpl
 *}
<table class="listing">
<tr class="heading">
<td colspan="2" width="40px" align="center">
{if $status == 'Setup' && count($factors) < $max_allowed_factors}
{submit_link action=addfactor src=$add_img}
{else}
{$addd_img}
{/if}
{$factors_tt|tts:""}
</td><td>Factor &#123;Levels&#125;</td><td width='40px'>Weight</td>
</tr>
{foreach from=$factors key=factor item=value name=forloop}
    {assign var='weight' value=$value[0]}
    {assign var='levels' value=$value[1]}
    {capture name=ls assign=commalevels}{foreach from=$levels item=level name=levelloop}{$level}{if not $smarty.foreach.levelloop.last}, {/if}{/foreach}{/capture}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td align='center' class='action'>
    {submit_link action=editfactor src=$edit_img factor=$factor initialfactor=$factor}
    </td>
    <td align="center" class="action">
    {if $status == 'Setup' and count($factors) > 1}
        {submit_link action=deletefactor src=$del_img factor=$factor}
    {else}
        {$deld_img}
    {/if}
    </td>
    <td>{$factor} &#123;{$commalevels}&#125;</td>
    <td>{$weight}</td></tr>
{/foreach}
</table>
