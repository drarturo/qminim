{*
 * trial_summary.tpl
 *}
<div align="center">
<h2>Trial summary{if $printonly eq 0} {submit_link action=printtrialsummary subpage="trial_summary.tpl" src=$printer_img}{else} <a href='{$base_url}'>{$home_img}</a>{/if}</h2>
<h3>Treatments</h3>
<table class="listing">
<tr class="heading">
<td>Treatment</td><td width="40px">Ratio</td>
</tr>
{foreach from=$treatments key=treatment item=ratio name=forloop}
<tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td>{$treatment}</td><td>{$ratio}</td>
</tr>
{/foreach}
</table>
<h3>Factors</h3>
<table class="listing">
<tr class="heading">
<td>Factor &#123;Levels&#125;</td><td width='40px'>Weight</td>
</tr>
{foreach from=$factors key=factor item=value name=forloop}
    {assign var='weight' value=$value[0]}
    {assign var='levels' value=$value[1]}
    {capture name=ls assign=commalevels}{foreach from=$levels item=level name=levelloop}{$level}{if not $smarty.foreach.levelloop.last}, {/if}{/foreach}{/capture}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td>{$factor} &#123;{$commalevels}&#125;</td>
    <td>{$weight}</td></tr>
{/foreach}
</table>
<h3>Settings</h3>
<table class='listing'>
<tr class='heading'>
<td>Setting Name</td><td>Setting Value</td></tr>
<tr valign='top' class='even'>
<td>Probability Method</td><td>{$prob_method}</td></tr>
<tr valign='top' class='odd'>
<td>Base Probability</td><td>{$base_prob}</td></tr>
<tr valign='top' class='even'>
<td>Distance Method</td><td>{$dist_method}</td></tr>
<tr valign='top' class='odd'>
<td>Sample Size</td><td>{$sample_size}</td></tr>
<tr valign='top' class='even'>
<td>Identifier Type</td><td>{$id_type}</td></tr>
<tr valign='top' class='odd'>
<td>Identifier Order</td><td>{$id_order}</td></tr>
<tr valign='top' class='even'>
<td>Date Initiated</td><td>{$date_initiated}</td></tr>
</table>
<h3>Preload</h3>
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
&nbsp;</th>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{$factor}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>Total</th>
</tr><tr>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {foreach from=$levels item=level}
        <th align='center'>{$level}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$preload key=treatment item=pfactors}
    <tr>
    <th align='left'>{$treatment}</th>
    {foreach from=$pfactors key=factor item=levels}
        {arr_sum|assign:s arr=$levels}
        {foreach from=$levels key=level_name item=level_value}
            <td align='center'>{$level_value}</td>
        {/foreach}
    {/foreach}
    <td align='center'>{$s}</td>
    </tr>
{/foreach}
<tr><th align='left'>Total</th>
{foreach from=$pgrand_total item=v}
    <td align='center'>{$v}</td>
{/foreach}
</tr></table>
<h3>Subjects ({$all_subjects_count})</h3>
<table class='listing'>
<tr valign='top' class='heading'>
<td width='50px'>ID</td>
<td>Treatment</td>
{foreach from=$factors key=factor item=v}
    <td>{$factor}</td>
{/foreach}
</tr>
{foreach from=$subjects key=id item=subject name=forloop}
    <tr valign="top" class="{if $smarty.foreach.forloop.index % 2 == 0}even{else}odd{/if}">
    <td>{$id}</td>
    <td>{$subject.treatment}</td>
    {foreach from=$factors key=factor item=values}
        <td>{$subject.$factor}</td>
    {/foreach}
    </tr>
{/foreach}
</table>
<h3>Frequencies</h3>
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
&nbsp;
</th>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{$factor}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>Total</th>
</tr><tr>
{foreach from=$factors key=factor item=value}
    {foreach from=$value[1] item=level}
        <th align='center'>{$level}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$frequencies key=treatment item=ffactors}
    <tr>
    <th align='left'>{$treatment}</th>
    {foreach from=$ffactors key=factor item=levels}
        {arr_sum|assign:s arr=$levels}
        {foreach from=$levels key=level_name item=level_value}
            <td align='center'>{$level_value}</td>
        {/foreach}
    {/foreach}
    <td align='center'>{$s}</td>
    </tr>
{/foreach}
<tr><th align='left'>Total</th>
{foreach from=$fgrand_total item=v}
    <td align='center'>{$v}</td>
{/foreach}
</tr></table>
</div>
