{*
 * edit_preloaddr.tpl
 *}
<table border='1' class='freq_table' cellspacing='0' width='100%'>
<tr>
<th rowspan='2' align='center' valign='middle'>
&nbsp;
</th>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {array_count|assign:cs arr=$levels}
    <th colspan='{$cs}' align='center'>{$factor}</th>
{/foreach}
<th rowspan='2' align='center' valign='middle'>Total</th>
</tr><tr>
{foreach from=$factors key=factor item=value}
    {assign var='levels' value=$value[1]}
    {foreach from=$levels item=level}
        <th align='center'>{$level}</th>
    {/foreach}
{/foreach}
</tr>
{foreach from=$preload key=treatment item=allfactors}
    <tr>
    <th align='left'>{$treatment}</th>
    {foreach from=$allfactors key=factor item=levels}
        {arr_sum|assign:s arr=$levels}
        {foreach from=$levels key=level_name item=level_value}
            <td align='center'>
            <input onChange="trim_value(this);" type='text' name='{$treatment}_{$factor}_{$level_name}' value='{$level_value}' size='1' onblur="update_preload(this);" /></td>
        {/foreach}
    {/foreach}
    <td align='center'><input type='text' readonly="readonly" name='{$treatment}_total' id='{$treatment}_total_id' value='{$s}' size='1' /></td>
    </tr>
{/foreach}
<tr><th align='left'>Total</th>
{foreach from=$pgrand_total key=fl item=v}
    <td align='center'><input type='text' readonly="readonly" id='total_{$fl}' value='{$v}' size='1' /></td>
{/foreach}
</tr>
</table>
