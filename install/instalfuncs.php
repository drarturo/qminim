<?php

/**
 * @file instalfuncs.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


function parent_writable($path) {
    if (file_exists($path))
        return is_writable($path);
    // if we are at the top file system level
    if (dirname(dirname($path)) == dirname($path))
        return false;
    return parent_writable(dirname($path));
}

function get_init_vars() {
    if ($_POST) {
        return $_POST;
    } else {
        $ret = array();
        $ret['data_dir'] = dirname(dirname(__FILE__)) . '/data/';
        $ret['cache_dir'] = dirname(dirname(__FILE__)) . '/cache/';
        $ret['sqlite_version'] = get_sqlite_version();
        $ret['admin_email'] = '';
        $ret['admin_username'] = '';
        $ret['admin_password'] = '';
        $ret['admin_confirm'] = '';
        return $ret;
    }
}

function get_sqlite_version() {
    switch (true) {
        case (class_exists("SQLite3")):
            return '3';
            break;
        case (class_exists("SQLiteDatabase")):
            return '2';
            break;
    }
}

function get_install_header() {
    $base_url = BASE_URL;
    $ret  = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>QMinim Online Minimization [install]</title>
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" language="javascript" src="{$base_url}js/overLib/overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script type="text/javascript" language="javascript" src="{$base_url}js/general.js"></script>
<link href="{$base_url}templates/styles/style.css" rel="stylesheet" type="text/css" />
</head>
<noscript>
    <h1>Your browser does not support javascript!</h1>
    <h1>Please enable javascript in your browser and try again.<h1>
    <style type="text/css">
        #wrap {display:none;}
    </style>
</noscript>
<body onload="focus_first_box();">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<div id="wrap">
<div id="header">
    <div id="header_left">
        <div id="header_right">
        <h1>QMinim Online Minimization<span class="subtitle">free solution</span><img id="logo"  src="{$base_url}templates/images/logo.gif" /></h1>
        </div><!-- div header right -->
    </div><!-- div header left -->
</div><!-- div header end -->
<div id="content">
<h3 align='center'>QMinim Installation!</h3>
EOF;
    return $ret;
}

function get_sqlite_radios($sqlite_version) {
    if ($sqlite_version == '3') {
        $sqlite_str = '<input type="radio" name="sqlite_version" value="3" checked="checked" />Version 3';
        $sqlite_str .= '<input type="radio" name="sqlite_version" value="2" />Version 2';
    } else {
        $sqlite_str = '<input type="radio" name="sqlite_version" value="3" />Version 3';
        $sqlite_str .= '<input type="radio" name="sqlite_version" value="2" checked="checked" />Version 2';
    }
    return $sqlite_str;
}

function get_form_str($init_vars) {
    foreach ($init_vars as $k => $v)
        $$k = $v;
    $sqlite_str = get_sqlite_radios($sqlite_version);
    $base_url = BASE_URL;
    $form_str = <<<EOF
<form action="{$base_url}install/install.php" method="post" onsubmit="return valid_install_form();">
<div align="center">
<table>
	<tr>
		<th colspan="3"><img src="{$base_url}templates/images/install.gif" alt="install" width="48" height="48" /></th>
	</tr>
	<tr>
	    <td colspan='3'><b>Important note! Please protect your data and cache folder from direct web access</b></td>
	</tr>
	<tr>
	    <td>Paths:</td>
		<td>data folder: </td>
		<td>
		<input onChange="trim_value(this);" type="text" name="data_dir" id="data_dir_id" value="{$data_dir}" size="36"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td>Cache folder: </td>
		<td>
		<input onChange="trim_value(this);" type="text" name="cache_dir" id="cache_dir_id" value="{$cache_dir}" size="36"/>
		</td>
	</tr>
	<tr>
	    <td>SQlite:</td>
		<td>Version: </td>
		<td>{$sqlite_str}</td>
	</tr>
	<tr>
	    <td>Admin:</td>
		<td>Email: </td>
		<td>
		<input onChange="trim_value(this);" type="text" name="admin_email" id="admin_email_id" value="{$admin_email}" size="36"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td>Username: </td>
		<td>
		<input onChange="trim_value(this);" type="text" name="admin_username" id="admin_username_id" value="{$admin_username}" size="36"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td>Password: </td>
		<td>
		<input onChange="trim_value(this);" type="password" name="admin_password" id="admin_password_id" value="{$admin_password}" size="36"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td>Confirm: </td>
		<td>
		<input onChange="trim_value(this);" type="password" name="admin_confirm" id="admin_confirm_id" value="{$admin_confirm}" size="36"/>
		</td>
	</tr>
	<tr>
	    <td>&nbsp;&nbsp;&nbsp;</td>
		<td colspan="2" align="center">
		<input type="submit" value="Install" />&nbsp;<input type="reset" />
		</td>
	</tr>
</table>
</div>
</form>
EOF;
return $form_str;
}

function get_install_footer() {
    $base_url = BASE_URL;
    $footer = <<<EOF
</div><!-- div content end -->
</div><!-- div wrap end -->
<div id="footer">
    <div class="separator" />
    <div id="footer_left">
        <div id="footer_right">
            <div class="foottext">
                <a href="http://www.php.net/"><img src="{$base_url}templates/images/php.gif" height="31"></a>
                <a href="http://www.sqlite.org/"><img src="{$base_url}templates/images/sqlite.gif" height="31"></a>
                <a href="http://www.smarty.net/"><img src="{$base_url}templates/images/smarty.gif" height="31"></a>
                <a href="http://www.bosrup.com/web/overlib/"><img src="{$base_url}templates/images/overlib.gif" height="31" alt="Popups by overLIB!" border="0" /></a>
                <a href="http://phpcaptcha.org/" target="_blank"><img src="{$base_url}templates/images/imgcap.gif" height="31" /></a>
            </div><!-- div foottext end -->
        </div>
    </div>
</div><!-- div footer end -->

</body>
</html>
EOF;
return $footer;
}

function check_writable($dir) {
    if (!parent_writable($dir)) {
        echo '<font color="red">' . $dir . ' is not writable !</font><br />';
        return false;
    }
    return true;
}

function create_folders($data_dir, $cache_dir) {
    $ret = true;
    if (!file_exists($data_dir)) {
        $ret = $ret && mkdir($data_dir, 0777, true);
    }
    if ($ret) {
        if (file_exists($data_dir . 'users.db')) {
            unlink($data_dir . 'users.db');
        }
        if (file_exists($data_dir . 'settings.db')) {
            unlink($data_dir . 'settings.db');
        }
    }
    if (!file_exists($cache_dir)) {
        $ret = $ret && mkdir($cache_dir, 0777, true);
    }
    if ($ret) {
        if (!file_exists($cache_dir . 't_cache')) {
            $ret = $ret && mkdir($cache_dir . 't_cache', 0777, true);
        } else {
            $ret = false;
            echo '<font color="red">' . $cache_dir . 't_cache' . ' is not writable !</font><br />';
        }
        if (!file_exists($cache_dir . 't_compile')) {
            $ret = $ret && mkdir($cache_dir . 't_compile', 0777, true);
        } else {
            $ret = false;
            echo '<font color="red">' . $cache_dir . 't_compile' . ' is not writable !</font><br />';
        }
        if (!file_exists($cache_dir . 't_config')) {
            $ret = $ret && mkdir($cache_dir . 't_config', 0777, true);
        } else {
            $ret = false;
            echo '<font color="red">' . $cache_dir . 't_config' . ' is not writable !</font><br />';
        }
        if (!file_exists($cache_dir . 'tmp')) {
            $ret = $ret && mkdir($cache_dir . 'tmp', 0777, true);
        } else {
            $ret = false;
            echo '<font color="red">' . $cache_dir . 'tmp' . ' is not writable !</font><br />';
        }
    }
    return $ret;
}

function terminate_page($form_str, $footer) {
    echo $form_str;
    echo $footer;
    exit();
}
?>
