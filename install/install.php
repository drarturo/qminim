<?php

/**
 * @file install.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */

if (!defined('BASE_URL')) {
    $base_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    $base_url = str_replace('install/', '', $base_url);
    $base_url = str_replace('install.php', '', $base_url);
    define('BASE_URL', $base_url); // set to the subfolder at the doc root
}

$uc_file = dirname(dirname(__FILE__)) . '/OUT_OF_SERVICE';
if (file_exists($uc_file)) {
    $uc = file_get_contents($uc_file);
    if (($uc - time()) > 0) {
        $return_date = date('l jS \of F Y h:i:s A', $uc);
        $content = file_get_contents(dirname(dirname(__FILE__)) . '/OUT_OF_SERVICE_TEMPLATE');
        echo sprintf($content, BASE_URL, $return_date);
        exit();
    }
}

if (file_exists(dirname(dirname(__FILE__)) . '/config.inc.php'))
    die('QMinim already installed. Delete the install folder and restart!<br />If you want to reinstall delete config.inc.file and try again!');
require_once(dirname(__FILE__) . '/instalfuncs.php');
$init_vars = get_init_vars();
foreach ($init_vars as $k => $v)
    $$k = $v;
echo get_install_header();
$form_str = get_form_str($init_vars);
$footer = get_install_footer();
if (!$_POST) terminate_page($form_str, $footer);
$writable = check_writable($data_dir);
$writable = check_writable($cache_dir) && $writable;
if (!$writable) terminate_page($form_str, $footer);
$ret = create_folders($data_dir, $cache_dir);
if (!$ret) terminate_page($form_str, $footer);

require_once('PasswordHash.php');
$t_hasher = new PasswordHash(8, FALSE);
$demo_pw_enc = $t_hasher->HashPassword('demodemo');
$pw_enc = $t_hasher->HashPassword($admin_password . $admin_username);
$p = strpos($pw_enc, '$', 1);
$hash_type = substr($pw_enc, 0, $p+1);
$demo_assoc = mt_rand(12345678,99999999);
$assoc = mt_rand(12345678,99999999);
$date_registered = date('Y-m-d H:i:s');
if (!defined('SQLITE_ASSOC'))
    define('SQLITE_ASSOC', 'SQLITE_ASSOC');
if (!defined('SQLITE_NUM'))
    define('SQLITE_NUM', 'SQLITE_NUM');
if (!defined('SQLITE_BOTH'))
    define('SQLITE_BOTH', 'SQLITE_BOTH');
require_once('db.php');
$db = new DB($data_dir . 'users.db', $sqlite_version);
$sql = file_get_contents(dirname(__FILE__) . '/sql/users.sql');
$sql = sprintf($sql, $db->quote($demo_pw_enc), $db->quote($date_registered), $db->quote($demo_assoc));
$params = array($admin_email, $admin_username, $pw_enc, $assoc, $date_registered);
$sql = $db->replace_params($sql, $params);
if (!$db->queryExec($sql, $error)) {
    echo $error;
    terminate_page($form_str, $footer);
}
$db = new DB($data_dir . 'settings.db', $sqlite_version);
$sql = file_get_contents(dirname(__FILE__) . '/sql/settings.sql');
$sql = sprintf($sql, $db->quote($hash_type));
$params = array($admin_username, 5);
$sql = $db->replace_params($sql, $params);
if (!$db->queryExec($sql, $error)) {
    echo $error;
    terminate_page($form_str, $footer);
}
$config_str = <<<EOD
<?php

if (!defined('GATE_PASSED')) exit(); 

if (!defined('SQLITE_ASSOC'))
    define('SQLITE_ASSOC', 'SQLITE_ASSOC');

if (!defined('SQLITE_NUM'))
    define('SQLITE_NUM', 'SQLITE_NUM');

if (!defined('SQLITE_BOTH'))
    define('SQLITE_BOTH', 'SQLITE_BOTH');

define('INSTALLED', true);
define('FILES_DIR', '{$data_dir}');
define('USERS_DB_FILE', FILES_DIR . 'users.db');
define('SETTINGS_DB_FILE', FILES_DIR . 'settings.db');
define('BASE_DIR', dirname(__FILE__) . '/');
define('CACHE_DIR', '{$cache_dir}'); // set to the absolute directory where cache folder located

define('SMARTY_DIR', BASE_DIR . 'lib/smarty/');

define('SWIFT_DIR', BASE_DIR . 'lib/swift/');

define('TEMPLATE_DIR', BASE_DIR . 'templates/');

?>
EOD;
$base_url = BASE_URL;
echo '<h4>Installation finished successfully!</h4>';
echo "<h3><a href='{$base_url}'>Login</a></h3>";
echo '<b>Save the following texts in a file named config.inc.php and place it in the root of the qminim folder</b><br />';
echo 'Important! Please do not forget to delete the install folder!';
echo '<textarea align="center" cols="120" rows="27" id="config_str_id" name="config_str">' . $config_str . '</textarea>';
echo $footer;
?>
