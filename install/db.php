<?php

/**
 * @file db.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */

class DB {

    var $db_file = null;
    var $db = null;
    var $sqlite_version = null;

    function DB($db_file, $sqlite_version) {
        if ($this->db_file != $db_file) {
            $this->db_file = $db_file;
            $this->sqlite_version = $sqlite_version;
            $this->init_db();
        }
    }
    
    function init_db() {
        if ($this->sqlite_version == '3' && class_exists('SQLite3')) {
            $this->db = new SQLite3($this->db_file, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
            $this->array_type = array(SQLITE_NUM => SQLITE3_NUM, SQLITE_ASSOC => SQLITE3_ASSOC, SQLITE_BOTH => SQLITE3_BOTH);
        } elseif ($this->sqlite_version == '2' && class_exists('SQLiteDatabase')) {
            $this->db = new SQLiteDatabase($this->db_file);
            $this->array_type = array(SQLITE_NUM => SQLITE_NUM, SQLITE_ASSOC => SQLITE_ASSOC, SQLITE_BOTH => SQLITE_BOTH);
        } else {
            die ('Sqlite version error!');
        }
    }

    function query($query, $arr_type, &$error) {
        if (!isset($this->db))
            $this->init_db();
        if ($this->sqlite_version == '3' && class_exists('SQLite3')) {
            while (!$this->db->busyTimeout(200));
            $stmt = $this->db->prepare($query);
            $result = $stmt->execute();
            if (!$result) {
                $error = $this->db->lastErrorMsg();
                return false;
            }
            $result_arr = array();
            while (!$this->db->busyTimeout(200));
            while($row = $result->fetchArray($this->array_type[$arr_type])) {
                $result_arr[] = (array) $row;
            }
            $result->finalize();
            $result = null;
            return new DBResultFactory($result_arr);
        } elseif ($this->sqlite_version == '2' && class_exists('SQLiteDatabase')) {
            $result = $this->db->query($query, $this->array_type[$arr_type], $error);
            if (!$result) {
                return false;
            }
            return $result;
        }
    }

    function replace_params($query, $params) {
        $rep = array_fill(0, count($params), '%s');
        $rep = implode(', ', $rep);
        $query = str_replace('###', $rep, $query);
        $params = array_map(array($this, 'quote'), $params);
        return vsprintf($query, $params);
    }

    function queryExec($query, &$error) {
        // $query must be parametrized
        if (!isset($this->db))
            $this->init_db();
        if ($this->sqlite_version == '3' && class_exists('SQLite3')) {
            try
            {
                $this->db->exec($query);
                $error = $this->db->lastErrorMsg();
                return ($this->db->lastErrorCode() == 0);
            } catch (Exception $error) {
                return false;
            }
        } elseif ($this->sqlite_version == '2' && class_exists('SQLiteDatabase')) {
            $this->db->queryExec($query, $error);
            return ($this->db->lastError() == 0);
        }
        return true;
    }

	public function quote($value)
	{
        if($this->sqlite_version=='3')
		{
			return "'" . $this->db->escapeString($value) . "'";
		}
		elseif($this->sqlite_version=='2')
		{
			return "'" . sqlite_escape_string($value) . "'";
		}
	}

}

class DBResultFactory {

    private $result;
    private $position = 0;

    function DBResultFactory($result) {
        $this->result = $result;
    }

    function fetch() {
        if ($this->position >= count($this->result)) return false;
        return $this->result[$this->position++];
    }
}

?>
