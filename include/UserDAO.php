<?php

/**
 * @file UserDAO.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

class UserDAO {

    var $user = array();

    /*  $data = array('field_name' => $field_name, 'field_value' => $field_value)
     *  e.g $data = array('field_name' => 'user_name', 'field_value' => 'mcarthur')
     *  selected field must be unique, it is string
     */
    function UserDAO($data) {
        $where = array();
        foreach($data as $pair) {
            $field = $pair['field_name'];
            $value = $pair['field_value'];
            $where[] = "{$field} = '{$value}'";
        }
        $where = implode(' AND ', $where);
        $query = "SELECT * FROM users WHERE {$where};";
        $db =& DB::getUserDB();
        if (!$result = $db->query($query, SQLITE_ASSOC, $error))
            die($error);
        if ($row = $result->fetch())
            $this->user = (array) $row;
    }

    public static function &getUserByUsername($user_name) {
        $user_dao = new UserDAO(array(array('field_name' => 'user_name', 'field_value' => $user_name)));
        return $user_dao->user;
    }

    public static function &getUserByEmail($email) {
        $user_dao = new UserDAO(array(array('field_name' => 'email', 'field_value' => $email)));
        return $user_dao->user;
    }
    
    public static function &getUser($fields) {
        $data = array();
        foreach ($fields as $name => $value) {
            $arr = array('field_name' => $name, 'field_value' => $value);
            $data[] = $arr;
        }
        $user_dao = new UserDAO($data);
        return $user_dao->user;
    }
}