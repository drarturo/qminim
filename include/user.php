<?php

/**
 * @file user.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function manage_activation_code_data($captcha_check=true) {
    global $smarty;
    $err_msg = validate_post(array('email'));
    if ($captcha_check and !check_captcha())
        $err_msg[] = "The security code entered was incorrect!";
    $body = "";
    foreach ($_POST as $key => $item) {
        $body .= "{$key}{$item}";
    }
    if (strip_custom($body) != $body or contains_bad_str($body))
        $err_msg[] = "Bad input!";
    if (!$err_msg) {
        $db = get_users_db();
        $email = $db->quote($_POST['email']);
        $ac = $db->quote('%,' . $_POST['activation_code']);
        $query = "SELECT count(*) FROM users WHERE email = $email AND activation_code LIKE $ac AND user_name IS NULL;";
        if($result = $db->query($query, SQLITE_NUM, $error)) {
            if ($row = $result->fetch()) {
                if ($row[0]) {
                    $smarty->assign('post', $_POST);
                    $smarty->assign('err_msg', '');
                    $smarty->assign('salutations', get_salutation());
                    $smarty->assign('job_titles', get_job_titles());
                    $smarty->assign('countries', get_countries());
                    $smarty->assign('eula', get_eula());
                    if (Settings::getSetting('register_captcha'))
                        $smarty->assign('show_captcha', true);
                    else
                        $smarty->assign('show_captcha', false);
                    $smarty->display('register.tpl');
                    return;
                } else {
                    $err_msg[] = "Wrong email or activation code!";
                }
            } else {
                // no row fetched
                if ($error)
                    die($error);
                $err_msg[] = "Wrong email or activation code!";
            }
        } else {
            die($error);
        }
    }
    if ($err_msg) {
        $smarty->assign('post', $_POST);
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        if (Settings::getSetting('activation_code_captcha'))
            $smarty->assign('show_captcha', true);
        else
            $smarty->assign('show_captcha', false);
        $smarty->display('activation_code.tpl');
    }
}


function manage_pre_register_data() {
    global $smarty;
    $err_msg = validate_post(array('email'));
    if (!check_captcha())
        $err_msg[] = "The security code entered was incorrect!";
    $body = "";
    foreach ($_POST as $key => $item) {
        $body .= "{$key}{$item}";
    }
    if (strip_custom($body) != $body or contains_bad_str($body))
        $err_msg[] = "Bad input!";
    if (!$err_msg) {
        if (get_user_var('activation_code', false)) {
            manage_activation_code_data(false);
            return;
        }
        $db = get_users_db();
        $query = "SELECT * FROM users WHERE email = %s;";
        $query = sprintf($query, $db->quote($_POST['email']));
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            if ($row = $result->fetch()) {
                if ($row['user_name']) {
                    $err_msg[] = "This email have been registered previously";
                } else {
                    if (!$err_msg) {
                        $smarty->assign('post', $_POST);
                        send_activation_code();
                        if (Settings::getSetting('activation_code_captcha'))
                            $smarty->assign('show_captcha', true);
                        else
                            $smarty->assign('show_captcha', false);
                        $smarty->display('activation_code.tpl');
                        return;
                    }
                }
            }
        } else {
            die($error);
        }
    }
    if ($err_msg) {
        $smarty->assign('post', $_POST);
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->display('pre_register.tpl');
    } else {
        send_activation_code();
        $smarty->assign('post', $_POST);
        if (Settings::getSetting('activation_code_captcha'))
            $smarty->assign('show_captcha', true);
        else
            $smarty->assign('show_captcha', false);
        $smarty->display('activation_code.tpl');
    }
}

function send_activation_code() {
    $updated = false;
    $db = get_users_db();
    $query = "SELECT * FROM users WHERE email = %s;";
    $query = sprintf($query, $db->quote($_POST['email']));
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        if ($row = $result->fetch()) {
            if ($row['user_name']) {
                header("Location: " . BASE_URL . 'index.php');
            } else {
                list($s, $activation_code) = explode(',', $row['activation_code']);
                $time_sent = (int) $s;
                if ($time_sent > 2) {
                    $activation_code = get_activation_code();
                    $time_sent = 0;
                }
                $time_sent += 1;
                $s = (string) $time_sent;
                $d = date('Y-m-d H:i:s');
                $query = "UPDATE users SET activation_code=%s, date_requested=%s WHERE email = %s;";
                $params = array($s . ',' . $activation_code, $d, $_POST['email']);
                $params = array_map(array($db, 'quote'), $params);
                $query = vsprintf($query, $params);
                if (!$db->queryExec($query, $error))
                    die($error);
                $updated = true;
            }
        }
    } else {
        die($error);
    }
    if (!$updated) {
        $activation_code = get_activation_code();
        $d = date('Y-m-d H:i:s');
        $disabled = Settings::getSetting('new_user_disabled');
        $query = "INSERT INTO users (email, disabled, activation_code, date_requested) VALUES (###);";
        $params = array($_POST['email'], $disabled, '1,' . $activation_code, $d);
        $query = $db->replace_params($query, $params);
        if (!$db->queryExec($query, $error))
            die($error);
    }
    $body = "We have recieved a request for a 'QMinim: Online Minimisation' account. If you have made this request please activate your email by entering the following code into the form in QMinim web site to continue with your registration.\n\n";
    $body .= "Please copy the activation code and goto to the QMinim website (" . BASE_URL . "), click on the register link and in the displayed page enter this activation code and your email address (" . $_POST['email'] . ").\n\nActivation code = {$activation_code}\n\nThank you\n\n";
    $body .= "QMinim Site Admin\n";
    $admin  = Settings::getTopAdmin();
    if (array_key_exists('email', $admin)) {
        $to = $admin['email'];
        $body .= $to . "\n\n";
    }
    $body .= "If this email is not related to you please ignore it, or inform the site admin\n\n";
    $subject = 'Activate your email: QMinim Online Minimisation';
    send_mail($subject, $body, $_POST['email']);
}

function get_activation_code() {
    mt_srand();
    return (string) mt_rand(123456, mt_getrandmax());
}

function manage_register_data () {
    global $smarty;
    $fields = array('salutation', 'job_title', 'first_name', 'last_name', 'country', 'city', 'email', 'affiliation', 'user_name', 'password', 'password_confirm', 'eulacheck');    
    $err_msg = validate_post($fields);
    if (!check_captcha())
        $err_msg[] = "The security code entered was incorrect!";
    $ip = $_SERVER['REMOTE_ADDR'];
    $body = "IP = $ip\n";
    $test_body = $ip;
    foreach ($fields as $item) {
        $test_body .= "{$item}" . $_POST[$item];
        $body .= "$item = " . $_POST[$item] . "\n";
    }
    if (strip_custom($test_body) != $test_body or contains_bad_str($test_body)) {
        $err_msg[] = "Bad input!";
    }

    if (!$err_msg) {
        $db = get_users_db();
        $query = "SELECT COUNT(*) FROM users WHERE email = %s AND user_name IS NOT NULL;";
        $query = sprintf($query, $db->quote($_POST['email']));
        if($result = $db->query($query, SQLITE_NUM, $error)) {
            if ($row = $result->fetch()) {
                if ($row[0] > 0)
                    $err_msg[] = "This email is in use. It seems that you have registered previously. Please contact admin if you forgot your password!";
            }
        } else {
            die($error);
        }
        $query = "SELECT COUNT(*) FROM users WHERE user_name = %s;";
        $query = sprintf($query, $db->quote($_POST['user_name']));
        if($result = $db->query($query, SQLITE_NUM, $error)) {
            if ($row = $result->fetch()) {
                if ($row[0] > 0)
                    $err_msg[] = "Username already in use. Select another username!";
            }
        } else {
            die($error);
        }
    }
    if ($err_msg) {
        $smarty->assign('post', post_subset($_POST, $fields));
	    $smarty->assign('salutations', get_salutation());
	    $smarty->assign('job_titles', get_job_titles());
	    $smarty->assign('countries', get_countries());
        $smarty->assign('eula', get_eula());
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        if (Settings::getSetting('register_captcha'))
            $smarty->assign('show_captcha', true);
        else
            $smarty->assign('show_captcha', false);
        $smarty->display('register.tpl');
    } else {
        extract($_POST);
        $date_registered = date('Y-m-d H:i:s');
        $disabled = Settings::getSetting('new_user_disabled');
        $jobs = get_job_titles();
        $job_title = $jobs[$job_title];
        $countries = get_countries();
        $country = $countries[$country];
        $password = hasher($password . $user_name);
        $assoc = mt_rand(12345678,99999999);
        if ($disabled) {
            $disabled_reason = "Your account has not been activated yet!";
            $registered_msg = "Your request will be considered by the site admin, and you will be informed of the result within 2-3 working days!";
            $extra_msg = 'Your request will be processed by the site admin, and you will be informed of the result within 2-3 working days!';
        } else {
            $disabled_reason = '';
            $registered_msg = "Please use your username and your password to log into QMinim!";
            $extra_msg = 'Please use your user name and your password to log into the qminim!';
        }

        $query = "DELETE FROM users where email = %s;";
        $query = sprintf($query, $db->quote($email));
        if (!$db->queryExec($query, $error))
            die($error);
        $query = "INSERT INTO users (salutation, job_title, first_name, last_name, email, user_name, password, affiliation, country, city, comment, date_registered, disabled, assoc, ip, disabled_reason) VALUES (###);";
        $params = array($salutation, $job_title, $first_name, $last_name, $email, $user_name, $password, $affiliation, $country, $city, $comment, $date_registered, $disabled, $assoc, $ip, $disabled_reason);
        $query = $db->replace_params($query, $params);
        if (!$db->queryExec($query, $error))
            die($error);
        $body = str_replace($_POST['password'], '', $body);
        $body = str_replace($_POST['password_confirm'], '', $body);
        $admin  = Settings::getTopAdmin();
        if (array_key_exists('email', $admin)) {
            $to = $admin['email'];
            send_mail('New registration', $body, $to);
        }
        $body = "Dear " . strip_custom($salutation) . " " . strip_custom($first_name) . " " . strip_custom($last_name) . "\n\n";
        $body .= "Thank you for registering with QMinim: Online Minimisation. $extra_msg\n\n";
        $body .= "QMinim Site Admin\n";
        if (array_key_exists('email', $admin))
            $body .= $to . "\n\n";
        $body .= "If this email is not related to you please ignore it, or inform the site admin\n\n";
        $subject = 'Registration with QMinim: Online Minimisation';
        send_mail($subject, $body, $email);
        $smarty->assign('registered_msg', $registered_msg);
        $smarty->display('registered.tpl');
    }
}

function validate_user() {
    global $smarty;
    if (isset($_SESSION['logintry'])) {
        $_SESSION['logintry']++;
    } else {
        $_SESSION['logintry'] = 1;
    }
    if (!check_captcha()) {
        $smarty->assign('security_error', true);
        $smarty->display('login.tpl');
        return;
    }
    if (isset($_POST['user_name']) && strlen($_POST['user_name']) > 0) {
        $user_name = $_POST['user_name'];
    } else {
        header("Location: " . BASE_URL . 'index.php');
        return;
    }
    if (isset($_POST['password']) && strlen($_POST['password']) > 0) {
        $password = $_POST['password'];
    } else {
        header("Location: " . BASE_URL . 'index.php');
        return;
    }
    $query = "SELECT * FROM users WHERE user_name = %s;";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($_POST['user_name']));
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        if(!$row = $result->fetch()) {
            $smarty->assign('login_error', true);
            $smarty->display('login.tpl');
            return;
        }
        if (!hasher($password . $user_name, $row['password'])) {
            $smarty->assign('login_error', true);
            $smarty->display('login.tpl');
            return;
        } else {
            unset($row['password']);
            if ($row['disabled']) {
                $smarty->assign('disabled_reason', $row['disabled_reason']);
                $smarty->display('disabled.tpl');
            } else {
                foreach($_SESSION as $k => $v)
                    unset($_SESSION[$k]);
                $query = "UPDATE users SET activation_code='' WHERE user_name = %s;";
                $query = sprintf($query, $db->quote($row['user_name']));
                if (!$db->queryExec($query, $error))
                    die($error);
                $_SESSION['userloggedin'] = (array) $row;
                $_SESSION['widescreen'] = $row['wide_screen'];
                if (Settings::isAdmin($row['user_name'])) {
                    $_SESSION['userloggedin']['is_admin'] = true;
                    $_SESSION['userloggedin']['rank'] = Settings::getAdminRank($row['user_name']);
                }
                $trial = get_trial_instance();
                header("Location: " . BASE_URL . 'index.php');
            }
        }
    } else {
        header("Location: " . BASE_URL . 'index.php');
    }
}

function manage_change_password() {
    global $smarty;
    $fields = array('current_password', 'new_password', 'new_password_confirm');
    $err_msg = validate_post($fields);

    $current_password = $_POST['current_password'];
    $new_password = $_POST['new_password'];
    $new_password_confirm = $_POST['new_password_confirm'];
    if ($new_password != $new_password_confirm)
        $err_msg[] = 'Passwords do not match!';
    if ($err_msg) {
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->display('change_password.tpl');
        return;
    }
    $user_name = $_SESSION['userloggedin']['user_name'];
    $query = "SELECT password FROM users WHERE user_name = %s;";
    $db = get_users_db();
    $query = sprintf($query, $db->quote($user_name));
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        $row = $result->fetch();
        if (!hasher($current_password . $user_name, $row['password'])) {
            $err_msg[] = 'Wrong current password!';
            $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
            $smarty->display('change_password.tpl');
            return;
        } else {
            $row['password'] = null;
            $hasher_pw = hasher($new_password . $user_name);
            $query = "UPDATE users SET password=%s WHERE user_name=%s;";
            $params = array($hasher_pw, $user_name);
            $params = array_map(array($db, 'quote'), $params);
            $query = vsprintf($query, $params);
            if (!$db->queryExec($query, $error))
                die($error);
            display_index_page('user.tpl');
        }
    } else {
        die($error);
    }
}

function manage_reset_password() {
    global $smarty;
    $fields = array('new_password', 'new_password_confirm');
    $err_msg = validate_post($fields);
    $new_password = $_POST['new_password'];
    $new_password_confirm = $_POST['new_password_confirm'];
    $user_name = $_POST['user_name'];
    if ($new_password != $new_password_confirm)
        $err_msg[] = 'Passwords do not match!';
    if ($err_msg) {
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->assign('user_name', $user_name);
        $smarty->display('reset_password_form.tpl');
        return;
    }
    $db = get_users_db();
    $hasher_pw = hasher($new_password . $user_name);
    $query = "UPDATE users SET password=%s, activation_code='' WHERE user_name=%s;";
    $params = array($hasher_pw, $user_name);
    $params = array_map(array($db, 'quote'), $params);
    $query = vsprintf($query, $params);
    if (!$db->queryExec($query, $error))
        die($error);
    $smarty->display('reset_password_success.tpl');
}

function send_password_reset_code($row) {
    global $smarty;
    $db = get_users_db();
    if ($row['activation_code']) {
        list($s, $activation_code) = explode(',', $row['activation_code']);
        $time_sent = (int) $s;
        if ($time_sent > 2) {
            $activation_code = get_activation_code();
            $time_sent = 0;
        }
    } else {
        $time_sent = 0;
        $activation_code = get_activation_code();
    }
    //if ($time_sent >= 8)
    //    header("Location: " . BASE_URL . 'index.php');
    $time_sent += 1;
    $s = (string) $time_sent;
    $d = date('Y-m-d H:i:s');
    $query = "UPDATE users SET activation_code=%s, date_requested=%s WHERE email = %s;";
    $params = array($s . ',' . $activation_code, $d, $row['email']);
    $params = array_map(array($db, 'quote'), $params);
    $query = vsprintf($query, $params);
    if (!$db->queryExec($query, $error))
        die($error);
    $body = "We have recieved a request to reset your password for 'QMinim: Online Minimisation' account. If you have made this request please complete the reset process by entering the following code into the form in QMinim web site.\n\n";
    $body .= "Please copy the reset code and goto to the QMinim website (" . BASE_URL . "), click on the 'Forget username or password?' link and in the displayed page enter this reset code and your email address (" . $row['email'] . ").\n\nReset code = {$activation_code}\n\nThank you\n\n";
    $body .= "QMinim Site Admin\n";
    $admin  = Settings::getTopAdmin();
    if (array_key_exists('email', $admin)) {
        $body .= $admin['email'] . "\n\n";
    }
    $body .= "Important!: If you have not requested reseting your password, please ignore ignore this email, or inform the site admin\n\n";
    $subject = 'Reset your password: QMinim Online Minimisation';
    send_mail($subject, $body, $row['email']);
    $smarty->assign("err_msg", "A reset password confirmation code was sent to your email address (" . $row['email'] . ").<br />Enter the reset code in the second box below to continue with the reset of your password!");
    $smarty->assign('post', $_POST);
    $smarty->display('login_problem.tpl');
}

function manage_sent_reset_code($row) {
    global $smarty;
    $err_msg = '';
    $db = get_users_db();
    $email = $db->quote($row['email']);
    $ac = $db->quote('%,' . $_POST['reset_code']);
    $query = "SELECT * FROM users WHERE email = $email AND activation_code LIKE $ac;";
    if($result = $db->query($query, SQLITE_ASSOC, $error)) {
        if ($row = $result->fetch()) {
            $query = "UPDATE users SET activation_code='' WHERE email=$email;";
            if (!$db->queryExec($query, $error))
                die($error);
            $smarty->assign('user_name', $row['user_name']);
            $smarty->display('reset_password_form.tpl');
            return;
        } else {
            $err_msg = "Wrong user_name/email or reset code!";
        }
    } else {
        die($error);
    }
    if ($err_msg) {
        $smarty->assign('post', $_POST);
        $smarty->assign("err_msg", "<div class='error'>{$err_msg}</div>");
        $smarty->display('login_problem.tpl');
    }
}

function manage_login_problem() {
    global $smarty;
    $err_msg = validate_post(array('user_name_or_email'));
    if (!check_captcha())
        $err_msg[] = "The security code entered was incorrect!";
    $body = "";
    foreach ($_POST as $key => $item) {
        $body .= "{$key}{$item}";
    }
    if (strip_custom($body) != $body or contains_bad_str($body))
        $err_msg[] = "Bad input!";
    if (!$err_msg) {
        $user_name_or_email = $_POST['user_name_or_email'];
        $db = get_users_db();
        $query = "SELECT * FROM users WHERE user_name = %s OR email = %s AND user_name IS NOT NULL;";
        $query = sprintf($query, $db->quote($user_name_or_email), $db->quote($user_name_or_email));
        if($result = $db->query($query, SQLITE_ASSOC, $error)) {
            if ($row = $result->fetch()) {
                if ($_POST['reset_code']) {
                    manage_sent_reset_code($row);
                    return;
                } else {
                    send_password_reset_code($row);
                    return;
                }
            } else {
                if (is_valid_email($user_name_or_email))
                    $err_msg = "Email {$user_name_or_email} not found!";
                else
                    $err_msg = "Username {$user_name_or_email} not found!";
                $smarty->assign("err_msg", "<div class='error'>{$err_msg}</div>");
                $smarty->assign("post", $_POST);
                $smarty->display('login_problem.tpl');
                return;
            }
        } else {
            die($error);
        }
    }
    if ($err_msg) {
        $smarty->assign('post', $_POST);
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->display('login_problem.tpl');
    } else {
        send_password_reset_code($row);
        $smarty->display('login_problem.tpl');
    }
}

function get_profile_data($user=null) {
    if (!$user)
        $user = $_SESSION['userloggedin'];
    $ret = $user;
    extract($user);
    $job_titles = get_job_titles();
    if (in_array($job_title, array_values($job_titles))) {
        $rev_titles = array_combine(array_values($job_titles), array_keys($job_titles));
        $job_title = $rev_titles[$job_title];
        $ret['job_title'] = $job_title;
    }
    $countries = get_countries();
    if (in_array($country, array_values($countries))) {
        $rev_countries = array_combine(array_values($countries), array_keys($countries));
        $country = $rev_countries[$country];
        $ret['country'] = $country;
    }
    return $ret;
}

function post_subset($source, $select) {
    $ret = array();
    foreach ($select as $sel) {
        if (in_array($sel, array_keys($source)))
            $ret[$sel] = htmlspecialchars($source[$sel]);
    }
    return $ret;
}

function manage_profile_data() {
    global $smarty;
    $fields = array('salutation', 'job_title', 'first_name', 'last_name', 'country', 'city', 'affiliation');
    $err_msg = validate_post($fields);
    $body = "";
    foreach ($fields as $item) {
        $body .= $_POST[$item];
    }
    if (strip_custom($body) != $body or contains_bad_str($body)) {
        $err_msg[] = "Bad input!";
    }

    if ($err_msg) {
        $smarty->assign('post', post_subset($_POST, $fields));
	    $smarty->assign('salutations', get_salutation());
	    $smarty->assign('job_titles', get_job_titles());
	    $smarty->assign('countries', get_countries());
        $smarty->assign("err_msg", '<div class="error">' . implode(', ', $err_msg) . '</div>');
        $smarty->display('edit_profile.tpl');
    } else {
        extract($_POST);
        $jobs = get_job_titles();
        $job_title = $jobs[$job_title];
        $countries = get_countries();
        $country = $countries[$country];

        $query = "UPDATE users set salutation=%s, job_title=%s, first_name=%s, last_name=%s, affiliation=%s, country=%s, city=%s WHERE user_name=%s;";
        $db = get_users_db();
        $params = array($salutation, $job_title, $first_name, $last_name, $affiliation, $country, $city, $_SESSION['userloggedin']['user_name']);
        $params = array_map(array($db, 'quote'), $params);
        $query = vsprintf($query, $params);
        if (!$db->queryExec($query, $error))
            die($error);
        $_SESSION['userloggedin']['salutation'] = $salutation;
        $_SESSION['userloggedin']['job_title'] = $job_title;
        $_SESSION['userloggedin']['first_name'] = $first_name;
        $_SESSION['userloggedin']['last_name'] = $last_name;
        $_SESSION['userloggedin']['affiliation'] = $affiliation;
        $_SESSION['userloggedin']['country'] = $country;
        $_SESSION['userloggedin']['city'] = $city;
        display_index_page('user.tpl');
    }
}

function toggle_user_screen() {
    global $smarty;
    $widescreen = 1 - $_SESSION['widescreen'];
    $query = "UPDATE users set wide_screen=%s WHERE user_name=%s;";
    $db = get_users_db();
    $params = array($widescreen, $_SESSION['userloggedin']['user_name']);
    $params = array_map(array($db, 'quote'), $params);
    $query = vsprintf($query, $params);
    if (!$db->queryExec($query, $error))
        die($error);
    $_SESSION['widescreen'] = $widescreen;
    $_SESSION['userloggedin']['wide_screen'] = $widescreen;
    if ($widescreen)
        $smarty->assign("set_width", true);
    else
        $smarty->assign("set_width", false);

    if ($_SESSION['widescreen']) {
        $smarty->assign("ws_img", 'templates/images/wide.gif');
    } else {
        $smarty->assign("ws_img", 'templates/images/narrow.gif');
    }

    display_index_page();
}

?>
