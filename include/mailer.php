<?php

/**
 * @file mailer.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit();

function send_mail($subject, $body, $to, $show_form=false, $params=false) {
    global $smarty;
    $to =  preg_split('/[;,]/', $to, -1, PREG_SPLIT_NO_EMPTY);
    $to = array_map('trim', $to);
    if ($show_form) {
        $smarty->assign('return_action', $show_form);
        $smarty->assign('params', $params);
        $smarty->assign('subject', $subject);
        $smarty->assign('body', $body);
        $smarty->assign('to', $to);
        $smarty->assign('cc', array(''));
        $smarty->assign('bcc', array(''));
        $smarty->display('email_form.tpl');
        return;
    }
    if (count($to) == 1)
        $to = $to[0];
    do_send_mail($subject, $body, $to);
}

function do_send_mail($subject, $body, $to, $cc=null, $bcc=null) {
    $message = Swift_Message::newInstance();
    $message->setSubject($subject);
    $message->setBody($body, 'text/plain', 'utf-8');

    $admin = Settings::getTopAdmin();
    if (array_key_exists('email', $admin)) {
        $from = array($admin['email'] => $admin['first_name'] . ' ' . $admin['last_name']);
        $message->setSender($admin['email']);
        $message->setFrom($from);
        $message->setReturnPath($admin['email']);
    }

    if ($to and is_array($to)) $to = array_filter($to, 'strlen');
    if ($to) $message->setTo($to);
    if ($cc and is_array($cc)) $cc = array_filter($cc, 'strlen');
    if ($cc) $message->setCc($cc);
    if ($bcc and is_array($bcc)) $bcc = array_filter($bcc, 'strlen');
    if ($bcc) $message->setBcc($bcc);
    if (! ($to || $cc || $bcc)) return;
    $mt = Settings::getSetting('mail_transfer');
    switch ($mt) {
        case 'smtp':
            $transport = Swift_SmtpTransport::newInstance(Settings::getSetting('smtp_server'), (int) Settings::getSetting('smtp_port'));
            $smtp_user = Settings::getSetting('smtp_user');
            $smtp_pw = Settings::getSetting('smtp_pw');
            if ($smtp_user)
                $transport->setUsername($smtp_user);
            if ($smtp_pw)
                $transport->setPassword($smtp_pw);
            break;
        case 'mail':
            $transport = Swift_MailTransport::newInstance();
            break;
    }
    $mailer = Swift_Mailer::newInstance($transport);
    if (!$mailer->send($message, $failures)) {
        $dt = date("Y-m-d H:i:s (T)");
        $err = "<errorentry>\n";
        $err .= "\t<datetime>" . $dt . "</datetime>\n";
        $err .= "\t<errortype>send_mail</errortype>\n";
        foreach ($failures as $failure) {
            $err .= "\t<failedsendto>" . $failure . "</failedsendto>\n";
        }
        $err .= "</errorentry>\n\n";
        error_log($err, 3, FILES_DIR . "error.log");
    }
}

function email_form_data() {
    global $smarty;
    if (isset($_POST['addTo']) || isset($_POST['addCc']) || isset($_POST['addBcc'])) {
        $smarty->assign('return_action', $_POST['return_action']);
        $params = array();
        foreach($_POST as $k => $v) {
            if (substr($k, 0, 7) == 'return_') {
                $key = substr($k, 7);
                $params[$key] = $v;
                unset($_POST[$k]);
                $_POST[$key] = $v;
            }
        }
        $smarty->assign('params', $params);
        $smarty->assign('subject', $_POST['subject']);
        $smarty->assign('body', $_POST['body']);
        $to = $_POST['to'];
        $cc = $_POST['cc'];
        $bcc = $_POST['bcc'];
        if (isset($_POST['addTo']))
            array_push($to, '');
        if (isset($_POST['addCc']))
            array_push($cc, '');
        if (isset($_POST['addBcc']))
            array_push($bcc, '');
        $smarty->assign('to', $to);
        $smarty->assign('cc', $cc);
        $smarty->assign('bcc', $bcc);
        $smarty->display('email_form.tpl');
        return;
    }
    if (isset($_POST['emailcanceled'])) {
        delete_user();
        show_site_users();
        return;
    }
    if (isset($_POST['emailsent'])) {
        $to = $_POST['to'];
        $cc = $_POST['cc'];
        $bcc = $_POST['bcc'];
        $body = $_POST['body'];
        $subject = $_POST['subject'];
        do_send_mail($subject, $body, $to, $cc, $bcc);
        show_site_users();
    }
}

?>
