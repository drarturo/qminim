<?php

/**
 * @file utils.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function titled($phrase, $glue=" ") {
    $words = preg_split("/[\s,_]+/", $phrase);
    $words = array_map('ucwords', $words);
    return implode($glue, $words);
}

function is_valid_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    //return preg_match('#^[a-z0-9.!\#$%&\'*+-/=?^_`{|}~]+@([0-9.]+|([^\s]+\.+[a-z]{2,6}))$#si', $email);
}

function get_int($v) {
    if (is_integer_numeric($v))
        return (int) $v;
    else
        return false;
}

function is_integer_numeric($v) {
    return filter_var($v, FILTER_VALIDATE_INT) !== false;
    //if (! is_numeric($v)) return false;
    //$ref = '0123456789';
    //for ($i=0; $i<strlen($v); $i++) {
    //    if (!strstr($ref, $v[$i])) return false;
    //}
    //return true;
}

function random_pw($len=null) {
    if (!$len)
        $len =& Settings::getSetting('rand_pw_len');
    $alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $ar = str_split($alphabet);
    $ar = array_combine($ar, $ar);
    $out = array_rand($ar, $len);
    return implode('', $out);
}

function get_mime_content_type($filename) {
    if (function_exists('mime_content_type')) {
        $result = mime_content_type($filename);
        if (($i = strpos($result, ';')) !== false) {
            $result = trim(substr($result, 0, $i));
        }
        return $result;
    } elseif (function_exists('finfo_open')) {
        $fi = finfo_open(FILEINFO_MIME);
		if ($fi !== false) {
			return strtok(finfo_file($fi, $filename), ' ;');
		}
    }
    $f = escapeshellarg($filename);
    $result = trim(`file --brief --mime $f`);
	if (($i = strpos($result, ';')) !== false) {
		$result = trim(substr($result, 0, $i));
	}
	return $result;
}

function valid_identifier($name) {
    $pattern = '/^[a-z][a-z0-9\-_:.]*$/i';
    return preg_match($pattern, $name);
}

function strip_custom($s) {
    if (!$s) return '';
    $allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/,.:@_-–()[]{} ";
    $allowed_chars .= chr(13);
    $allowed_chars .= chr(10);
    $ret = "";
    foreach(str_split($s) as $c) {
        if (strstr($allowed_chars, $c))
            $ret .= $c;
    }
    return $ret;
}

?>