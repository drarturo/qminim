<?php

/**
 * @file test_trial.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */



if (!defined('GATE_PASSED')) exit(); 

require_once('minim.php');

class TestTrial {

    var $db;
    var $db_file;
    var $treatments;
    var $factors;
    var $prob_method;
    var $base_prob;
    var $dist_method;
    var $preload;
    var $subjects;
    var $frequencies;
    
    function TestTrial($db_file) {
        $this->inited = true;
        $this->db_file = $db_file;
        try {
            $this->init_trial();
        } catch (Exception $e) {
            $this->inited = false;
            $this->error = 'Invalid trial file';
        }
    }

    function init_trial() {
        $this->init_db();
        if (!$this->db) {
            $this->inited = false;
            $this->error = 'Invalid trial file';
            return;
        }
        $this->init_treatments();
        $this->init_factors();
        $this->init_settings();
        $this->init_preload();
        $this->init_subjects();
        $this->update_frequencies();
        $this->set_status();
    }

    function set_status() {
        $this->status = 'Setup';
        if ($this->set_status_finished())
            return;
        if ($this->set_status_enroll())
            return;
        array_walk_recursive($this->preload, array($this, 'set_status_preload'));
    }

    function set_status_preload($load, $key) {
        if ($load > 0)
            $this->status = 'Preload';
    }

    function set_status_enroll() {
        if (count($this->subjects) > 0) {
            $this->status = 'Enroll';
            return true;
        }
        return false;
    }

    function set_status_finished() {
        if (count($this->subjects) == $this->sample_size) {
            $this->status = 'Finished';
            return true;
        }
        return false;
    }

    function get_levels($factor) {
        $levels = array();
        $query = "SELECT level FROM levels WHERE factor=%s;";
        $query = sprintf($query, $this->db->quote($factor));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $levels[] = $row['level'];
            }
            return $levels;
        } else {
            $this->error_function($error);
        }
    }
    
    function init_subject_levels(&$subject) {
        $subject_id = $subject['id'];
        $query = "SELECT factor, level FROM subject_levels WHERE subject_id=%s;";
        $query = sprintf($query, $this->db->quote($subject_id));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                if ($row['factor'] == 'treatment') continue;
                $subject[$row['factor']] = $row['level'];
            }
        } else {
            $this->error_function($error);
        }
    }

    function update_frequencies() {
        $this->frequencies = $this->preload;
        foreach ($this->subjects as $id => $subject) {
            $t = $subject['treatment'];
            foreach ($this->factors as $f => $v) {
                $l = $subject[$f];
                $this->frequencies[$t][$f][$l]++;
            }
        }
        $this->balance = $this->get_trial_balance();
    }

    function get_minim_model() {
        $minim = new Minim($this);
        $minimum_treatment = array_search(min($this->treatments), $this->treatments);
        $minim->minimum_treatment = $minimum_treatment;
        $minim->prob_method = $this->prob_method;
        $minim->dist_method = $this->dist_method;
        $minim->base_prob = $this->base_prob;
        $minim->build_probs();
        return $minim;
    }

    function get_subject($id) {
        $query = "SELECT * FROM subjects WHERE id=%s;";
        $query = sprintf($query, $this->db->quote($id));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            $row = $result->fetch();
            $subject = array();
            $subject['id'] = $row['id'];
            $subject['treatment'] = $row['treatment'];
            $this->init_subject_levels($subject);    
            $subject['enrolled_by'] = $row['enrolled_by'];
            $subject['date_enrolled'] = $row['date_enrolled'];
            $subject['date_modified'] = $row['date_modified'];
        } else {
            $this->error_function($error);
        }
        return $subject;
    }

    function init_treatments() {
        $this->treatments = array();
        $query = "SELECT * FROM treatments;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $this->treatments[$row["treatment"]] = $row["ratio"];
            }
        } else {
            $this->error_function($error);
        }
    }

    function init_factors() {
        $this->factors = array();
        $query = "SELECT * FROM factors;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $this->factors[$row["factor"]] = array($row["weight"], $this->get_levels($row["factor"]));
            }
        } else {
            $this->error_function($error);
        }
    }

    function init_settings() {
        $query = "SELECT * FROM settings;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            $row = $result->fetch();
            $this->prob_method = $row['prob_method'];
            $this->base_prob = $row['base_prob'];
            $this->dist_method = $row['dist_method'];
            $this->sample_size = $row['sample_size'];
            $this->id_type = $row['id_type'];
            $this->id_order = $row['id_order'];
        } else {
            $this->error_function($error);
        }
    }

    function get_preload_transpose() {
        $preloadtp = array();
        $t = array();
        foreach ($this->factors as $factor => $value) {
            $preloadtp[$factor] = array();
            $levels = $value[1];
            foreach ($levels as $level) {
                $preloadtp[$factor][$level] = array();
                foreach ($this->treatments as $treatment => $ratio) {
                    if (! array_key_exists($treatment, $t))
                        $t[$treatment] = 0;
                    $t[$treatment] += $this->preload[$treatment][$factor][$level];
                    $preloadtp[$factor][$level][$treatment] = $this->preload[$treatment][$factor][$level];
                }
            }
        }
        foreach($t as $treatment => $p) {
            $t[$treatment] = $p / count($this->factors);
        }
        $preloadtp['Total'] = $t;
        return $preloadtp;
    }

    function get_frequencies_transpose() {
        $frequenciestp = array();
        $t = array();
        foreach ($this->factors as $factor => $value) {
            $frequenciestp[$factor] = array();
            $levels = $value[1];
            foreach ($levels as $level) {
                $frequenciestp[$factor][$level] = array();
                foreach ($this->treatments as $treatment => $ratio) {
                    if (! array_key_exists($treatment, $t))
                        $t[$treatment] = 0;
                    $t[$treatment] += $this->frequencies[$treatment][$factor][$level];
                    $frequenciestp[$factor][$level][$treatment] = $this->frequencies[$treatment][$factor][$level];
                }
            }
        }
        foreach($t as $treatment => $p) {
            $t[$treatment] = $p / count($this->factors);
        }
        $frequenciestp['Total'] = $t;
        return $frequenciestp;
    }

    function init_preload() {
        $a = array();
        foreach ($this->treatments as $t => $r) {
            $a[$t] = array();
            foreach ($this->factors as $f => $v) {
                $a[$t][$f] = array();
                foreach ($v[1] as $l) {
                    $a[$t][$f][$l] = 0;
                }
            }
        }
        $this->preload = $a;
        $query = "SELECT * FROM preload;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $t = $row['treatment'];
                $f = $row['factor'];
                $l = $row['level'];
                $this->preload[$t][$f][$l] = $row['total'];
            }
        } else {
            $this->error_function($error);
        }
        $this->set_status();
    }

    function init_subjects() {
        $this->subjects = array();
        $query = "SELECT * FROM subjects;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $subject = array();
                foreach($row as $k => $v) {
                    $subject[$k] = $v;
                }
                $this->init_subject_levels($subject);
                $this->subjects[$row['id']] = $subject;
            }
        } else {
            $this->error_function($error);
        }
    }

    function init_db() {
        try {
            if (!$this->db = new DB($this->db_file))
                $this->error_function('Invalid trial file');
            $query = "SELECT date_initiated FROM settings;";
            if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
                $row = $result->fetch();
                $this->date_initiated = $row['date_initiated'];
            } else {
                $this->error_function($error);
            }
        } catch (Exception $e) {
            $this->error_function($e);
        }
    }

    function error_function($error){
        $this->inited = false;
        $this->error = $error;
    }

    function get_treatments_summary() {
        $ret = array();
        foreach ($this->treatments as $treatment => $ratio) {
            $ret[] = "$treatment => $ratio";
        }
        return "Treatments: " . implode(', ', $ret);
    }

    function get_factors_summary() {
        $ret = array();
        foreach ($this->factors as $factor => $value) {
            $ret[] = "$factor => " . $value[0] . ", (" . implode(', ', $value[1]) . ")";
        }
        return "Factors: " . implode('; ', $ret);
    }

    function get_summary() {
        if (!$this->treatments || !$this->factors) return false;
        $ret = array();
        $ret[] = $this->get_treatments_summary();
        $ret[] = $this->get_factors_summary();
        $ret[] = "Probability Method: " . $this->prob_method;
        $ret[] = "Base Probability: " . $this->base_prob;
        $ret[] = "Distance Method: " . $this->dist_method;
        $ret[] = "Sample Size: " . $this->sample_size;
        $ret[] = "Identifier Type: " . $this->id_type;
        $ret[] = "Identifier Order: " . $this->id_order;
        $ret[] = "Number of subjects: " . count($this->subjects);
        $ret[] = "Date Initiated: " . $this->date_initiated;
        return $ret;
    }

    function get_trial_balance() {
        $levels = array();
        $balance = array();
        foreach ($this->frequencies as $treatment => $factors) {
            foreach ($factors as $factor => $ls) {
                $levels[$factor] = array();
                foreach ($ls as $level => $v) {
                    $levels[$factor][$level] = array();
                }
            }
        }
        foreach ($levels as $factor => $ls) {
            $balance[$factor] = array();
            $balance[$factor]['total'] = array('Marginal Balance' => 0, 'Range' => 0, 'Variance' => 0, 'SD' => 0);
            foreach ($ls as $level => $v) {
                $balance[$factor][$level] = array();
                foreach ($this->frequencies as $treatment => $factors) {
                    $levels[$factor][$level][$treatment] = 1.0 * $this->frequencies[$treatment][$factor][$level] / $this->treatments[$treatment];
                }
            }
        }
        $means = array('Marginal Balance' => 0, 'Range' => 0, 'Variance' => 0, 'SD' => 0);
        $maxes = array('Marginal Balance' => 0, 'Range' => 0, 'Variance' => 0, 'SD' => 0);
        $m = 0;
        foreach ($levels as $factor => $ls) {
            foreach ($ls as $level => $cnt) {
                $mb = $this->get_marginal_balance($cnt);
                $mb *= $this->factors[$factor][0];
                $balance[$factor][$level]['Marginal Balance'] = $mb;
                $rng = max(array_values($cnt)) - min(array_values($cnt));
                $rng *= $this->factors[$factor][0];
                $balance[$factor][$level]['Range'] = $rng;
                $variance = $this->get_variance($cnt);
                $variance *= $this->factors[$factor][0];
                $balance[$factor][$level]['Variance'] = $variance;
                $sd = $this->get_standard_deviation($cnt);
                $sd *= $this->factors[$factor][0];
                $balance[$factor][$level]['SD'] = $sd;
                foreach ($balance[$factor][$level] as $stat => $statv) {
                    $means[$stat] += $balance[$factor][$level][$stat];
                    if ($maxes[$stat] < $balance[$factor][$level][$stat]) {
                        $maxes[$stat] = $balance[$factor][$level][$stat];
                    }
                    $balance[$factor]['total'][$stat] += $balance[$factor][$level][$stat];
                }
                $m++;
            }
            foreach ($balance[$factor]['total'] as $stat => $statv) {
                $balance[$factor]['total'][$stat] /= (1.0 * count($levels[$factor]));
            }
        }
        foreach ($means as $stat => $statv) {
            $means[$stat] /= (1.0 * $m);
        }
        $balance['Mean'] = $means;
        $balance['Max'] = $maxes;
        return $this->format_balance($balance);
    }

    function format_balance($balance) {
        $table = array();
        foreach($this->factors as $factor => $v) {
            $row = array($factor, '');
            $factor_balance = $balance[$factor];
            $total_balance = $factor_balance['total'];
            $row[] = $total_balance['Marginal Balance'];
            $row[] = $total_balance['Range'];
            $row[] = $total_balance['Variance'];
            $row[] = $total_balance['SD'];
            $table[] = $row;
            foreach($v[1] as $level) {
                $row = array('', $level);
                $level_balance = $factor_balance[$level];
                $row[] = $level_balance['Marginal Balance'];
                $row[] = $level_balance['Range'];
                $row[] = $level_balance['Variance'];
                $row[] = $level_balance['SD'];
                $table[] = $row;
            }
        }
        $row = array('Mean', '');
        $mean_balance = $balance['Mean'];
        $row[] = $mean_balance['Marginal Balance'];
        $row[] = $mean_balance['Range'];
        $row[] = $mean_balance['Variance'];
        $row[] = $mean_balance['SD'];
        $table[] = $row;
        $row = array('Max', '');
        $max_balance = $balance['Max'];
        $row[] = $max_balance['Marginal Balance'];
        $row[] = $max_balance['Range'];
        $row[] = $max_balance['Variance'];
        $row[] = $max_balance['SD'];
        $table[] = $row;
        return $table;
    }

    function get_grand_total($load) {
       $grand_total = array();
        foreach ($this->factors as $factor => $value) {
            foreach ($value[1] as $level) {
                $grand_total[$factor . '-' . $level] = 0;
            }
        }
        $grand_total['total'] = 0;
        foreach ($load as $treatment => $factors) {
            foreach ($factors as $factor => $levels) {
                $s = array_sum($levels);
                foreach ($levels as $level_name => $level_value) {
                    $grand_total[$factor . '-' . $level_name] += $level_value;
                }
            }
            $grand_total['total'] += $s;
        }
        return $grand_total;
    }

    function get_marginal_balance($cnt) {
        $combinatorics = new Math_Combinatorics;
        $com_counts = $combinatorics->combinations($cnt, 2);
        $a = array();
        foreach ($com_counts as $com_count) {
            $c = array_values($com_count);
            $a[] = abs($c[0] - $c[1]);
        }
        $numerator = array_sum($a);
        $denominator = (count($cnt)-1) * array_sum(array_values($cnt));
        if ($denominator == 0) return 0.0;
        return (1.0 * $numerator) / $denominator;
    }
    
    function get_variance($cnt) {
        $mean = (1.0 * array_sum(array_values($cnt))) / count($cnt);
        $sq_terms_arr = array();
        foreach(array_values($cnt) as $i) {
            $sq_terms_arr[] = pow(($i-$mean), 2);
        }
        $sq_terms = array_sum($sq_terms_arr);
        return (1.0 * $sq_terms) / (count($cnt) - 1.0);
    }
    
    function get_standard_deviation($cnt) {
        return sqrt($this->get_variance($cnt));
    }    

    function valid_subjects_levels() {
        $query = "SELECT * FROM subject_levels;";
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            $subject_levels = $result->numRows();
            return $subject_levels - (count($this->factors) * count($this->subjects));
        } else {
            return false;
        }
    }

    function refresh_subjects_levels() {
        $query = array();
        $query[] = "DELETE FROM subject_levels";
        foreach ($this->subjects as $id => $subject) {
            foreach ($this->factors as $f => $l) {
                $level = $subject[$f];
                $q = "INSERT INTO subject_levels (subject_id, factor, level) " .
                "VALUES (###)";
                $params = array($id, $f, $level);
                $query[] = $this->db->replace_params($q, $params);
            }
        }
        if (count($query)) {
            $query = implode(';', $query);
            if(!$this->db->queryExec($query, $error))
            {
              die($error);
            }
        }
    }

    function get_subjects_limited($offset, $rowsperpage) {
        $subjects = array();
        $query = "SELECT * FROM subjects LIMIT %s, %s;";
        $query = sprintf($query, $this->db->quote($offset), $this->db->quote($rowsperpage));
        if($result = $this->db->query($query, SQLITE_ASSOC, $error)) {
            while($row = $result->fetch()) {
                $subject = array();
                foreach($row as $k => $v) {
                    $subject[$k] = $v;
                }
                $this->init_subject_levels($subject);
                $subjects[$row['id']] = $subject;
            }
        } else {
            $this->error_function($error);
        }
        return $subjects;
    }

    public static function &getTrial($db_file, $refresh=false) {
        static $instance;
		if (!isset($instance) or $refresh) {
			$instance = new Trial($db_file);
		}
		return $instance;
    }
}
?>
