<?php

/**
 * @file preload.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function preload_edited() {
    global $smarty;
    $trial = get_trial_instance();
    $save_msg = array();
    $preload = array();
    foreach ($trial->treatments as $treatment => $ratio) {
        $check_arr = array();
        $preload[$treatment] = array();
        foreach ($trial->factors as $factor => $v) {
            $preload[$treatment][$factor] = array();
            foreach ($v[1] as $level) {
                $name = $treatment . '_' . $factor . '_' . $level;
                $name = str_replace(" ", "_", $name);
                $value = get_int($_POST[$name]);
                if ($value === false) {
                    $nme = str_replace("_", " ", $name);
                    $save_msg[] = "Only integer values are acceptable (${nme}: $_POST[$name])!";
                    $value = $_POST[$name];
                }
                $preload[$treatment][$factor][$level] = $value;
            }
            $check_arr[] = array_sum($preload[$treatment][$factor]);
        }
        if (count(array_unique($check_arr)) > 1) {
            $save_msg[] = "Sum of level counts are not consistent accross factors for ${treatment}!";
        }
    }
    if (!$save_msg) {
        $trial->update_preload($preload);
        display_index_page('preload.tpl');
    } else {
        $smarty->assign("factors", $trial->factors);
        $smarty->assign("treatments", $trial->treatments);
        $smarty->assign("preload", $preload);
        $smarty->assign("save_msg", implode(', ', $save_msg));
        $smarty->assign("pgrand_total", $trial->get_grand_total($trial->preload));
        $smarty->display('edit_preload.tpl');
    }
}

function save_as_preload() {
    $trial = get_trial_instance();
    $trial->save_as_preload();
    display_index_page('preload.tpl');
}

?>