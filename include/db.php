<?php

/**
 * @file db.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit();

class DB {

    var $db_file = null;
    var $db = null;

    public static function &getSettingsDB() {
        static $instance;
		if (!isset($instance)) {
			$instance = new DB(SETTINGS_DB_FILE);
		}
		return $instance;
    }

    public static function &getTrialDB($db_file) {
        static $instance;
		if (!isset($instance)) {
			$instance = new DB($db_file);
		}
		return $instance;
    }

    public static function &getUserDB() {
        static $instance;
		if (!isset($instance)) {
			$instance = new DB(USERS_DB_FILE);
		}
		return $instance;
    }

    function DB($db_file) {
        if (file_exists($db_file)) {
            if (! DB::check_db_file($db_file)) {
                die($db_file . ': ' . $_SESSION['userloggedin']['user_name'] . ': your database file is not a valid sqlite db file!');
            }
        }
        if ($this->db_file != $db_file) {
            $this->db_file = $db_file;
            $this->init_db();
        }
    }

    public static function check_db_file($file) {
        $con = strtolower(file_get_contents($file, NULL, NULL, 0, 60));
        if (strpos($con, "** this file contains an sqlite 2.1 database **", 0) !== false) {
            if (class_exists('SQLiteDatabase'))
                return true;
        }
        if (strpos($con, "sqlite format 3", 0) !== false) {
            if (class_exists('SQLite3'))
                return true;
        }
        return false;
    }

    function init_db() {
        if (class_exists('SQLite3')) {
            $this->db = new SQLite3($this->db_file, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
            $this->array_type = array(SQLITE_NUM => SQLITE3_NUM, SQLITE_ASSOC => SQLITE3_ASSOC, SQLITE_BOTH => SQLITE3_BOTH);
            $this->type = "SQLite3";
        } elseif (class_exists('SQLiteDatabase')) {
            $this->db = new SQLiteDatabase($this->db_file);
            $this->array_type = array(SQLITE_NUM => SQLITE_NUM, SQLITE_ASSOC => SQLITE_ASSOC, SQLITE_BOTH => SQLITE_BOTH);
            $this->type = "SQLiteDatabase";
        }
    }

	//returns an array of arrays after doing a SELECT
	public function query($query, $mode, &$error)
	{
		$mode = $this->array_type[$mode];
		if($this->type=="SQLite3")
		{
		    $result = $this->db->query($query);
            if ($this->db->lastErrorCode()) {
                $error = $this->db->lastErrorMsg();
                return NULL;
            }
		    if(!$result) //make sure the result is valid
			    return NULL;
			$arr = array();
			$i = 0;
			while($res = $result->fetchArray($mode))
			{
				$arr[$i] = $res;
				$i++;
			}
			return new DBResultFactory($arr);
		}
		elseif($this->type=="SQLiteDatabase")
		{
		    $result = $this->db->query($query, $mode, $error);
		    if(!$result) //make sure the result is valid
			    return NULL;
			return $result;
		}
	}

    function replace_params($query, $params) {
        $rep = array_fill(0, count($params), '%s');
        $rep = implode(', ', $rep);
        $query = str_replace('###', $rep, $query);
        $params = array_map(array($this, 'quote'), $params);
        return vsprintf($query, $params);
    }

    function queryExec($query, &$error) {
        if (!isset($this->db))
            $this->init_db();
        if($this->type=="SQLite3")
		{
			$success = $this->db->exec($query);
			if(!$success) $error = $this->db->lastErrorMsg();
		}
		elseif ($this->type == "SQLiteDatabase")
		{
			$success = $this->db->queryExec($query, $error);
		}
		if(!$success)
		{
			$error = "Error in query: '" . $error . "'";
			return false;
		}
		else
		{
			return true;	
		}
    }

	//correctly escape an identifier (column / table / trigger / index name) to be injected into an SQL query
	public function quote_id($value)
	{
		// double-quotes need to be escaped by doubling them
		$value = str_replace('"','""',$value);
		return '"'.$value.'"';
	}

	//correctly escape a string to be injected into an SQL query
	public function quote($value)
	{
		if ($this->type == "SQLite3")
		{
			return "'".$this->db->escapeString($value)."'";
		}
		else if ($this->type == "SQLiteDatabase")
		{
			return "'".sqlite_escape_string($value)."'";
		}
	}

}

class DBResultFactory {

    private $result;
    private $position = 0;

    function DBResultFactory($result) {
        $this->result = $result;
    }

    function fetch() {
        if ($this->position >= count($this->result)) return false;
        return $this->result[$this->position++];
    }
    
    function numRows() {
        return count($this->result);
    }
}

?>
