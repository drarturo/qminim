<?php

/**
 * @file factor.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function factor_added() {
    global $smarty;
    $trial = get_trial_instance();
    $save_msg = array();
    $factor = $_POST['factor'];
    $invalid_names = array('treatment', 'treatments', 'subjects', 'factors', 'levels', 'subject_levels');
    if (in_array(strtolower($factor), $invalid_names) || ! valid_identifier($factor))
        $save_msg[] = "Invalid factor name '$factor' !";
    $weight = strip_custom($_POST['weight']);
    $levels = explode(",", strip_custom($_POST['levels']));
    $levels = array_map('trim', $levels);
    if (count($levels) < 2) {
        $save_msg[] = "More levels needed!";
    }
    if (count($levels) > Settings::getSetting('max_allowed_levels')) {
        $save_msg[] = "Too many levels! Maximum = " . Settings::getSetting('max_allowed_levels');
    }
    $check = array();
    foreach ($levels as $level) {
        if (in_array($level, $check)) {
            $save_msg[] = "Duplicate level!";
            break;
        }
        $check[] = $level;
    }
    if ((float) $weight <= 0 )
        $save_msg[] = "Invalid weight!";
    if (array_key_exists($factor, $trial->factors))
        $save_msg[] = "Duplicate factor!";
    if ($save_msg) {
        $save_msg = implode(', ', $save_msg);
        $smarty->assign("status", $trial->status);
        $smarty->assign('factor', $factor);
        $smarty->assign('weight', $weight);
        $smarty->assign('levels', $levels);
        $smarty->assign('save_msg', $save_msg);
        $smarty->display('add_factor.tpl');
    } else {
        $trial->insert_factor($factor, (float) $weight, $levels);
        display_index_page('factors.tpl');
    }
}

function levels_incompatible($old_levels, $new_levels) {
    $a = array();
    for ($i=0; $i<count($old_levels); $i++) {
        if ($old_levels[$i] != $new_levels[$i])
            if (in_array($new_levels[$i], $old_levels))
                $a[] = $new_levels[$i];
    }
    return implode(', ', $a);
}

function factor_edited() {
    global $smarty;
    $trial = get_trial_instance();
    $save_msg = array();
    $factor = $_POST['factor'];
    $invalid_names = array('treatment', 'treatments', 'subjects', 'factors', 'levels', 'subject_levels');
    if (in_array(strtolower($factor), $invalid_names) || ! valid_identifier($factor))
        $save_msg[] = "Invalid factor name!";
    $initialfactor = strip_custom($_POST['initialfactor']);
    $weight = strip_custom($_POST['weight']);
    $levels = explode(",", strip_custom($_POST['levels']));
    $levels = array_map('trim', $levels);
    if (count($levels) < 2) {
        $save_msg[] = "More levels needed!";
    }
    if (count($levels) > Settings::getSetting('max_allowed_levels')) {
        $save_msg[] = "Too many levels! Maximum = " . Settings::getSetting('max_allowed_levels');
    }
    if (count($trial->subjects))
        if ($incomp_levels = levels_incompatible($trial->factors[$initialfactor][1], $levels))
            $save_msg[] = "Incompatible old and new levels! (${incomp_levels})";
    $check = array();
    foreach ($levels as $level) {
        if (in_array($level, $check)) {
            $save_msg[] = "Duplicate level!";
            break;
        }
        $check[] = $level;
    }
    if ((float) $weight <= 0 )
        $save_msg[] = "Invalid weight!";
    if (array_key_exists($factor, $trial->factors) and $factor != $initialfactor)
        $save_msg[] = "Duplicate factor!";
    if ($save_msg) {
        $save_msg = implode(', ', $save_msg);
        $smarty->assign('factor', $factor);
        $smarty->assign('initialfactor', $initialfactor);
        $smarty->assign('weight', $weight);
        $smarty->assign('levels', $levels);
        $smarty->assign('save_msg', $save_msg);
        $smarty->display('edit_factor.tpl');
    } else {
        $trial->update_factor($initialfactor, $factor, (float) $weight, $levels);
        display_index_page('factors.tpl');
    }
}

function delete_factor() {
    global $smarty;
    $trial = get_trial_instance();
    $factor = strip_custom($_POST['factor']);
    $trial->delete_factor($factor);
    display_index_page('factors.tpl');
}

?>
