<?php

/**
 * @file smarty_funcs.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */


if (!defined('GATE_PASSED')) exit(); 

function submit_form($params, &$smarty) {
    $action = $params['action'];
    unset($params['action']);
    $same_case = false;
    if (array_key_exists('same_case', $params)) {
        $same_case = $params['same_case'];
        unset($params['same_case']);
    }
    $curclass = '';
    if (array_key_exists('currenttemplate', $params)) {
        $currenttemplate = $params['currenttemplate'];
        unset($params['currenttemplate']);
        if (array_key_exists('subpage', $params)) {
            $subpage = $params['subpage'];
            if ($currenttemplate == $subpage) {
                $curclass = ' current';
            }
        }
    }
    if (isset($params['alt'])) {
        $a = $params['alt'];
        unset($params['alt']);
    }
    if (array_key_exists('label', $params)) {
        $label = $params['label'];
        if (!isset($a)) {
            $a = $label;
        }
        unset($params['label']);
    }
    if (array_key_exists('src', $params)) {
        $src = $params['src'];
        unset($params['src']);
        if (!isset($a)) {
            $p = strrpos($src, '/');
            $a = substr($src, $p+1, strlen($src));
            $p = strrpos($a, '.');
            $a = substr($a, 0, $p);
        }
    }
    $alt = '';
    if (isset($a)) {
        $a = ucfirst($a) . ' ' . str_replace($a, '', $action);
        $a = str_replace('index_page', '', $a);
        $a = str_replace('set_subjects_filter_from_freq', '', $a);
        $a = str_replace('_page', '', $a);
        $a = str_replace('_screen_toggle', '', $a);        
        $a = str_replace('Narrow wide', 'Narrow', $a);
        $a = str_replace(' toggle_include_preload', '', $a);
        
        $alt = " alt='$a' title='$a'";
    }
    $confirm_msg='';
    if (array_key_exists('confirm_msg', $params)) {
        $confirm_msg = ", '" . $params['confirm_msg'] . "'";
        unset($params['confirm_msg']);
    }
    $tag = 'button';
    if (array_key_exists('tag', $params)) {
        $tag = $params['tag'];
        unset($params['tag']);
    }
    $obj = array();
    foreach ($params as $k => $v) {
        $obj[] = "$k:'$v'";
    }
    $obj = "{" . implode(',', $obj) . "}";

    switch ($tag) {
        case 'a':
            $class = 'clicker action' . $curclass;
            if ($same_case)
                $class = 'clicker' . $curclass;
            $ret = "<span class='${class}' onclick=\"submit_form('${action}', ${obj}${confirm_msg});\"$alt>$label</span>";
            break;
        case 'button':
            if (isset($label)) {
                $ret = "<input type='button' value= '${label}' onclick=\"submit_form('${action}', ${obj}${confirm_msg});\"$alt />";
            } else {
                $ret = "<input type='image' align='bottom' src='${src}' onclick=\"submit_form('${action}', ${obj}${confirm_msg});\"$alt />";
            }
            break;
    }
    return $ret;
}

function get_paging_block($params, &$smarty) {
    global $smarty;
    $numrows = $params['numrows'];
    if ($numrows == 0) return '';
    $totalpages = ceil($numrows / Settings::getSetting('rows_per_page'));
    $currentpage = $params['currentpage'];
    $start = (($currentpage - 1) * Settings::getSetting('rows_per_page')) + 1;
    $end = $currentpage * Settings::getSetting('rows_per_page');
    if ($end > $numrows) $end = $numrows;
    if ($totalpages < 2) return "<table width='100%'><tr><td align='center'><b>Subject ${start} to ${end} (${numrows} total)</b></td></tr></table>";
    $link = "";
    $link_arr = array('currentpage' => 1, 'action' => 'index_page');
    if ($currentpage > 1) {
        $link_arr['src'] = "templates/images/first.gif";
        $link = ' ' . submit_form($link_arr, $smarty) . ' ';
        $prevpage = $currentpage - 1;
        $link_arr['currentpage'] = $prevpage;
        $link_arr['src'] = "templates/images/previous.gif";
        $link .= ' ' . submit_form($link_arr, $smarty) . ' ';
    }
    for ($x = ($currentpage - Settings::getSetting('rows_per_page')); $x < (($currentpage + Settings::getSetting('rows_per_page')) + 1); $x++) {
        if (($x > 0) && ($x <= $totalpages)) {
            if ($x == $currentpage) {
                $link .= " [ <b>$x</b> ] ";
            } else {
                $link_arr['tag'] = 'a';
                $link_arr['currentpage'] = $x;
                $link_arr['label'] = "$x";
                $link .= ' ' . submit_form($link_arr, $smarty) . ' ';
            }
        }
    }
    unset($link_arr['tag']);
    unset($link_arr['label']);
    if ($currentpage != $totalpages) {
        $nextpage = $currentpage + 1;
        $link_arr['currentpage'] = $nextpage;
        $link_arr['src'] = "templates/images/next.gif";
        $link .= ' ' . submit_form($link_arr, $smarty) . ' ';
        $link_arr['currentpage'] = $totalpages;
        $link_arr['src'] = "templates/images/last.gif";
        $link .= ' ' . submit_form($link_arr, $smarty) . ' ';
    }
    if ($link) {
        $link = "<div class='pagination'>$link</div>";
    }
    return "<table width='100%'><tr><td align='left'>${link}</td><td align='center'><b>Subject ${start} to ${end} (${numrows} total)</b></td><td align='right'>${link}</td></tr></table>";
}

function assign_tooltips() {
    global $smarty;
    $text = trim(file_get_contents(BASE_DIR . 'include/tool_tips.dat'));
    $text = explode('$$', $text);
    foreach($text as $tip) {
        list($k, $l) = explode('::', $tip);
        $smarty->assign($k . "_tt", $l);
    }
}

function common_smarty_assign() {
    global $smarty;
    if (isset($_SESSION['userloggedin'])) {
        $smarty->assign("user", $_SESSION['userloggedin']);
        if (isset($_SESSION['loginas']))
            $smarty->assign("loginas", $_SESSION['loginas']);
        if ($_SESSION['userloggedin']['user_name'] == 'demo')
            $smarty->assign("demo", true);
    }    
    $subpage = null;
    if (isset($_POST['subpage']))
        $subpage = $_POST['subpage'];

    if (isset($_SESSION['logintry'])) {
    $max_tries = (int) Settings::getSetting('nocaptcha_login_retry');
    if ($_SESSION['logintry'] > $max_tries)
        $smarty->assign('show_captcha', true);
    }
    $admin = Settings::getTopAdmin();
    if (array_key_exists('email', $admin))
        $smarty->assign("admin_email", $admin['email']);
    $smarty->assign("subpage", $subpage);
    $smarty->assign("rand_pw_len", Settings::getSetting('rand_pw_len'));
    $smarty->assign("register_disabled", Settings::getSetting('register_disabled', 0));
    $smarty->assign("max_allowed_treatments", Settings::getSetting('max_allowed_treatments'));
    $smarty->assign("max_allowed_subjects", Settings::getSetting('max_allowed_subjects'));
    $smarty->assign("max_allowed_factors", Settings::getSetting('rows_per_page'));
    $smarty->assign("max_allowed_levels", Settings::getSetting('max_allowed_levels'));
    $smarty->assign("max_allowed_chars", Settings::getSetting('max_allowed_chars'));
    $smarty->assign("max_register_chars", Settings::getSetting('max_register_chars'));    
    $smarty->assign("max_comment_chars", Settings::getSetting('max_comment_chars'));
    $smarty->assign("show_footer_external_link", Settings::getSetting('show_footer_external_link'));
    assign_tooltips();
    if (Settings::getSetting('wide_screen_default'))
        $smarty->assign("signup_img", 'templates/images/signup0.gif');
    else
        $smarty->assign("signup_img", 'templates/images/signup.gif');
    $followus = <<<EOF
<!-- AddThis Follow BEGIN -->
<div class="addthis_toolbox addthis_32x32_style addthis_default_style">
<a class="addthis_button_linkedin_follow" addthis:userid="qminim" addthis:usertype="company"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=msaghaei"></script>
<!-- AddThis Follow END -->
EOF;
    $smarty->assign("followus", $followus);
    $addthiscode = <<<EOF
<!-- AddThis Button BEGIN -->
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=msaghaei"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=msaghaei"></script>
<!-- AddThis Button END -->
EOF;
    if (Settings::getSetting('addthiscode', 0))
        $smarty->assign("addthiscode", $addthiscode);
    else
        $smarty->assign("addthiscode", '');
    $smarty->assign("noadmin_img", 'templates/images/noadmin.gif');
    $smarty->assign("admin_img", 'templates/images/admin.gif');
    $smarty->assign("increase_img", 'templates/images/bcollapse.gif');
    $smarty->assign("decrease_img", 'templates/images/bexpand.gif');
    $smarty->assign("repair_img", 'templates/images/repair.gif');
    $smarty->assign("enabled_img", 'templates/images/enabled.gif');
    $smarty->assign("disabled_img", 'templates/images/disabled.gif');
    $smarty->assign("edit_img", 'templates/images/edit.gif');
    $smarty->assign("add_img", 'templates/images/add.gif');
    $smarty->assign("del_img", 'templates/images/delete.gif');
    $smarty->assign("clear_img", 'templates/images/clear.gif');
    $smarty->assign("editd_img", '<img src="templates/images/editd.gif" />');
    $smarty->assign("addd_img", '<img src="templates/images/addd.gif" />');
    $smarty->assign("deld_img", '<img src="templates/images/deleted.gif" />');
    $smarty->assign("enroll_all_img", 'templates/images/enroll_all.gif');
    $smarty->assign("transposedr_img", 'templates/images/transposedr.gif');
    $smarty->assign("transposetp_img", 'templates/images/transposetp.gif');
    $smarty->assign("printer_img", 'templates/images/printer.gif');
    $smarty->assign("home_img", '<img src="templates/images/home.gif" />');
    $smarty->assign("refresh_img", '<img src="templates/images/refresh.png" width="16" height="16" />');
    $smarty->assign("captcha_img", '<img border="1" id="captcha" src="' . BASE_URL . 'lib/securimage/securimage_show.php" alt="CAPTCHA Image" />');
    $smarty->assign("save_as_preload_img", 'templates/images/save_as_preload.gif');
    $smarty->assign("astrisk", '<span class="error">*</span>');
    $smarty->assign("login_as_img", 'templates/images/login_as.gif');
    $smarty->assign("msg_img", 'templates/images/msg.gif');
    $smarty->assign("selected_img", '<img src="templates/images/selected.gif" />');
    $smarty->assign("select_img", 'templates/images/select.gif');
    $smarty->assign("download_img", 'templates/images/download.gif');
    $smarty->assign("upload_img", 'templates/images/upload.gif');
    if (isset($_SESSION['widescreen']) and $_SESSION['widescreen']) {
        $smarty->assign("ws_img", 'templates/images/wide.gif');
    } else {
        $smarty->assign("ws_img", 'templates/images/narrow.gif');
    }
    if (isset($_POST['xpos'])) {
        $smarty->assign("xpos", $_POST['xpos']);
        $smarty->assign("ypos", $_POST['ypos']);
    }
    if (isset($_POST['scroll_to_end'])) {
        $smarty->assign("scroll_to_end", $_POST['scroll_to_end']);
    }
    $smarty->assign("menu_img", "<img src='templates/images/menu.gif'>");
    $smarty->assign("menu_red_img", "<img src='templates/images/menu_red.gif'>");
}

function smarty_assign($value, $varName, $passThru = false) {
    global $smarty;
    if (isset($varName)) {
        $smarty->assign($varName, $value);
    }
    if ($passThru) return $value;
}

function get_array_count($params, &$smarty) {
    return count($params['arr']);
}

function get_array_sum($params, &$smarty) {
    return array_sum($params['arr']);
}

function tooltipsticky($text, $link="") {
    list($k, $text) = explode('##', $text);
    $icon = "<img src='templates/images/info.gif' alt='Info' title='Info' />";
    $js = "<a href='javascript:void(0);' onclick='return overlib(\"%s\", STICKY, CAPTION, \"%s \", RIGHT);' onmouseout='return nd();'>%s%s</a>";
    $ret = sprintf($js, $text, $k, $link, $icon);
    return $ret;
}

function selectmenu($params, &$smarty) {
    $treatments = $params['selected_treatments'];
    $factors = $params['selected_factors'];
    $ret ="<a href='javascript:void(0);' onclick='toggle_display(\"item_select_div_id\")'><img src='templates/images/menu.gif' alt='Filter menu' title='Filter menu' /></a><div id='item_select_div_id'>";
    $cols = count($factors) + 1;
    $ret .= "<table border='0' cellspacing='0'><tr><th colspan='{$cols}' align='center'>Subjects filter</th></tr><tr valign='center'><td>Treatments</td>";
    foreach ($factors as $f => $levels) {
        $ret .= "<td>$f</td>";
    }
    $ret .= "</tr><tr valign='top'><td>";

    $cb = array();
    $ts = array();
    $total_cb = count($treatments);
    $n = 0;
    foreach ($treatments as $t => $v) {
        $checked = '';
        if ($v) {
            $checked = " checked";
            $n += 1;
        }
        $cb[] = "<input type='checkbox' name='selected_treatments_{$t}' id='selected_treatments_{$t}_id' value='$t'{$checked}/>$t";
        $ts[] = $t;
    }
    $cb = implode('<br />', $cb);
    $ret .= "{$cb}</td>";

    $fs = array();

    foreach ($factors as $f => $levels) {
        $total_cb += count($levels);
        $cb = array();
        foreach ($levels as $l => $v) {
            $checked = '';
            if ($v) {
                $checked = " checked";
                $n += 1;
            }
            $level = str_replace($f . '_', '', $l);
            $level_strip = str_replace(' ', '$', $l);
            $cb[] = "<input type='checkbox' name='selected_factor_{$level_strip}' id='selected_factor_{$level_strip}_id' value='{$level_strip}'{$checked}/>$level";
            $fs[] = "{$level_strip}";
        }
        $cb = implode('<br />', $cb);
        $ret .= "<td>{$cb}</td>";
    }
    if ($n < $total_cb)
        $ret = str_replace('menu.gif', 'menu_red.gif', $ret);
    $colspan = (int) ($cols / 2);
    $ret .= "</tr><tr><td colspan='$colspan' align='center'>";
    $ts = implode(',', $ts);
    $fs = implode(',', $fs);
    if ((2 * $colspan) < $cols)
        $colspan += 1;
    $ret .= "<input type='button' class='refresh' value='Apply' onclick='submit_select_form(\"$ts\", \"$fs\")' /></td><td colspan='$colspan' align='right'>";
    $ret .= "&nbsp;<input type='button' class='refresh' value='Close' onclick='toggle_display(\"item_select_div_id\")'>";
    $ret .= "</td></tr></table></div>";
    return $ret;
}

function subjectprop($params, &$smarty) {
    $subject = $params['subject'];
    $text = "<ul>";
    $text .= sprintf("<li>ID: %s</li>", $subject['id']);
    $text .= sprintf("<li>Treatment: %s</li>", $subject['treatment']);
    $a = array('id', 'treatment', 'enrolled_by', 'date_enrolled', 'date_modified'); 
    foreach ($subject as $k => $v) {
        if (in_array($k, $a)) continue;
        if (is_numeric($k)) continue;
        $text .= "<li>$k: $v</li>";
    }
    $text .= sprintf("<li>Enrolled by: %s</li>", $subject['enrolled_by']);
    $text .= sprintf("<li>Date enrolled: %s</li>", $subject['date_enrolled']);
    $text .= sprintf("<li>Date modified: %s</li>", $subject['date_modified']);
    $text .= "</ul>";
    $icon = "<img src='templates/images/prop.gif' alt='Properties' title='Properties' />";
    $attr = array("FGCOLOR, \"#CCCCFF\"",
                   "BGCOLOR, \"#333399\"",
                   "TEXTCOLOR, \"#000000\"",
                   "CAPCOLOR, \"#FFFFFF\"",
                   "CLOSECOLOR, \"#9999FF\"",
                   "TEXTFONT, \"Verdana,Arial,Helvetica\"",
                   "TEXTSIZE, \"1\"",
                   "CAPTIONSIZE, \"1\"",
                   "CLOSESIZE, \"1\"",
                   "WIDTH, \"300\"",
                   "BORDER, \"1\"",
                   "CLOSETITLE, \"X\"",
                   "CAPICON, \"templates/images/prop.gif\"",
                  );
    $attr = implode(', ', $attr);
    $js = "<a href='javascript:void(0);' onclick='return overlib(\"%s\", STICKY, CAPTION, \"%s\", RIGHT, %s);' onmouseout='return nd();'>%s</a>";
    $ret = sprintf($js, $text, "Subject " . $subject['id'], $attr, $icon);
    return $ret;
}

function userprop($params, &$smarty) {
    $user = $params['user'];
    extract($user);
    $text = "User information:<ul>";
    $text .= sprintf("<li>Salutation: %s</li>", $salutation);
    $text .= sprintf("<li>Job title: %s</li>", $job_title);
    $text .= sprintf("<li>First name: %s</li>", $first_name);

    $text .= sprintf("<li>Last name: %s</li>", $last_name);
    $text .= sprintf("<li>Email: %s</li>", $email);
    $text .= sprintf("<li>User name: %s</li>", $user_name);
    $text .= sprintf("<li>Affiliation: %s</li>", $affiliation);
    $text .= sprintf("<li>Country: %s</li>", $country);
    $text .= sprintf("<li>City: %s</li>", $city);
    $text .= sprintf("<li>IP: %s</li>", $ip);
    if ($disabled) {
        $text .= sprintf("<li>Disabled: %s</li>", $disabled_reason);
    }
    $text .= "</ul>";
    $db_file = FILES_DIR . "trial_{$email}_${assoc}.sqlite";
    $redundant_error = '';
    if (file_exists($db_file)) {
        $text .= "DB file:<ul>";
        if (DB::check_db_file($db_file)) {
            try {
                $test_trial = new TestTrial($db_file);
                if (!$test_trial) {
                    $text .= "<li>ERROR DB</li>";
                } else {
                    $ret = $test_trial->get_summary();
                    foreach($ret as $item) {
                        $text .= "<li>$item</li>";
                    }
                    $redundant_subject_levels = $test_trial->valid_subjects_levels();
                    if ($redundant_subject_levels != 0)
                        $redundant_error = '*';
                    $text .= "<li>Extra levels: {$redundant_subject_levels}";
                }
            } catch (Exception $e) {
                $text .= "<li>ERROR DB: $e</li>";
            }
            $text .= "<li>" . sprintf("File size: %.2f KB", filesize($db_file)/1024) . "</li>";
            $text .= "<li>Last accessed: " . date("F d Y H:i:s.", fileatime($db_file)) . "</li>";
            $text .= "<li>Last modified: " . date("F d Y H:i:s.", filemtime($db_file)) . "</li>";
            $text .= "</ul>";
        } else {
            $text .= "<li>invalid db</li></ul>";
            $redundant_error = '**';
        }
    }    
    $icon = "<img src='templates/images/prop.gif' alt='Properties' title='Properties' />";
    $attr = array("FGCOLOR, \"#CCCCFF\"",
                   "BGCOLOR, \"#333399\"",
                   "TEXTCOLOR, \"#000000\"",
                   "CAPCOLOR, \"#FFFFFF\"",
                   "CLOSECOLOR, \"#9999FF\"",
                   "TEXTFONT, \"Verdana,Arial,Helvetica\"",
                   "TEXTSIZE, \"1\"",
                   "CAPTIONSIZE, \"1\"",
                   "CLOSESIZE, \"1\"",
                   "WIDTH, \"300\"",
                   "BORDER, \"1\"",
                   "CLOSETITLE, \"X\"",
                   "CAPICON, \"templates/images/prop.gif\"",
                  );
    $attr = implode(', ', $attr);
    if ($redundant_error == '*') {
        $confirm_msg = ", 'Repair user {$user_name} DB file?'";
        $obj = "{db_file:'$db_file'}";
        $re = "<input type='image' align='bottom' src='templates/images/repair_db.gif' onclick=\"submit_form('repair_user_db', ${obj}${confirm_msg}); \" alt='Repair user DB' title='Repair user DB' />";
    } else {
        $re = $redundant_error;
    }
    $js = "<a href='javascript:void(0);' onclick='return overlib(\"%s\", STICKY, CAPTION, \"%s\", RIGHT, %s);' onmouseout='return nd();'>%s</a>{$re}";
    $ret = sprintf($js, $text, "$salutation $first_name $last_name", $attr, $icon);
    return $ret;
}

?>
