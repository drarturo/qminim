<?php

/**
 * @file config.inc.template.php
 *
 * Copyright (c) 2012-2013 Mahmoud Saghaei
 * Distributed under the GNU GPL v3. For full terms refer to http://www.gnu.org/copyleft/gpl.html.
 *
 */



if (!defined('GATE_PASSED')) exit(); 

if (!defined('SQLITE_ASSOC'))
    define('SQLITE_ASSOC', 'SQLITE_ASSOC');; 

if (!defined('SQLITE_NUM'))
    define('SQLITE_NUM', 'SQLITE_NUM');; 

if (!defined('SQLITE_BOTH'))
    define('SQLITE_BOTH', 'SQLITE_BOTH');

define('INSTALLED', true);
define('FILES_DIR', '/path/to/qminim/data/');
define('USERS_DB_FILE', FILES_DIR . 'users.db');
define('SETTINGS_DB_FILE', FILES_DIR . 'settings.db');
define('BASE_DIR', dirname(__FILE__) . '/');
define('BASE_URL', 'http://localhost/path/to/qminim/'); // set to the subfolder at the doc root
define('CACHE_DIR', '/path/to/qminim/cache/'); // set to the absolute directory where cache folder located
//define('OUT_OF_SERVICE', true); // uncomment for out of service (maintenance)

define('SMARTY_DIR', BASE_DIR . 'lib/smarty/');

define('SWIFT_DIR', BASE_DIR . 'lib/swift/');

define('TEMPLATE_DIR', BASE_DIR . 'templates/');

?>
